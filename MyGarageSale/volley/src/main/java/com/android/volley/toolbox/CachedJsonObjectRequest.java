/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.volley.toolbox;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.http.impl.cookie.DateParseException;
import org.apache.http.impl.cookie.DateUtils;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.NetworkResponseEx;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.CachedJsonObjectRequest.CachedJsonObject;

/**
 * A request for retrieving a {@link JSONObject} response body at a given URL, allowing for an
 * optional {@link JSONObject} to be passed in as part of the request body.
 */
public class CachedJsonObjectRequest extends JsonRequest<CachedJsonObject> {
	
	/**
	 * Creates a new request.
	 * @param method the HTTP method to use
	 * @param url URL to fetch the JSON from
	 * @param jsonRequest A {@link JSONObject} to post with the request. Null is allowed and
	 *   indicates no parameters will be posted along with request.
	 * @param listener Listener to receive the JSON response
	 * @param errorListener Error listener, or null to ignore errors.
	 */
	public CachedJsonObjectRequest(int method, String url, CachedJsonObject jsonRequest,
			Listener<CachedJsonObject> listener, ErrorListener errorListener) {
		super(method, url, (jsonRequest == null) ? null : ((jsonRequest.getJsonObject() == null) ? null : jsonRequest.getJsonObject().toString()), listener, errorListener);
		this.setShouldResultCachedResponse(jsonRequest.isCachedResponse());
	}

	@Override
	protected Response<CachedJsonObject> parseNetworkResponse(NetworkResponse response) {
		try {
			boolean isCachedResponse = false;
			if (response instanceof NetworkResponseEx) {
				isCachedResponse = ((NetworkResponseEx) response).isCachedResponse;
			}
			final String jsonString =
					new String(response.data, HttpHeaderParser.parseCharset(response.headers));
			JSONObject jsonObject = new JSONObject(jsonString);
			return Response.success(new CachedJsonObject(isCachedResponse, jsonObject),
					CachedJsonObjectRequest.parseCacheHeaders(response));
		} catch (final UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (final JSONException je) {
			return Response.error(new ParseError(je));
		}
	}

	/**
	 * Extracts a {@link Cache.Entry} from a {@link NetworkResponse}.
	 *
	 * @param response The network response to parse headers from
	 * @return a cache entry for the given response, or null if the response is not cacheable.
	 */
	public static Cache.Entry parseCacheHeaders(NetworkResponse response) {
		long now = System.currentTimeMillis();

        Map<String, String> headers = response.headers;

        long serverDate = 0;
        long serverExpires = 0;
        long softExpire = 0;
        long maxAge = 0;
        boolean hasCacheControl = false;

        String serverEtag = null;
        String headerValue;

        headerValue = headers.get("Date");
        if (headerValue != null) {
            serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }

        headerValue = headers.get("Cache-Control");
        if (headerValue != null) {
            hasCacheControl = true;
            String[] tokens = headerValue.split(",");
            for (int i = 0; i < tokens.length; i++) {
                String token = tokens[i].trim();
                if (token.equals("no-cache") || token.equals("no-store")) {
                    return null;
                } else if (token.startsWith("max-age=")) {
                    try {
                        maxAge = Long.parseLong(token.substring(8));
                    } catch (Exception e) {
                    }
                } else if (token.equals("must-revalidate") || token.equals("proxy-revalidate")) {
                    maxAge = 0;
                }
            }
        }

        headerValue = headers.get("Expires");
        if (headerValue != null) {
            serverExpires = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }

        serverEtag = headers.get("ETag");

        // Cache-Control takes precedence over an Expires header, even if both exist and Expires
        // is more restrictive.
        if (hasCacheControl) {
            softExpire = now + maxAge * 1000;
        } else if (serverDate > 0 && serverExpires >= serverDate) {
            // Default semantic for Expire header in HTTP specification is softExpire.
            softExpire = now + (serverExpires - serverDate);
        }

        Cache.Entry entry = new Cache.Entry();
        entry.data = response.data;
        entry.etag = serverEtag;
        entry.softTtl = softExpire;
        // changed ttl to 2 days
        entry.ttl = System.currentTimeMillis() + 2 * 24 * 60 * 60 * 1000;
        entry.serverDate = serverDate;
        entry.responseHeaders = headers;

        return entry;
	}

	public static class CachedJsonObject {

		private boolean isCachedResponse;
		private JSONObject jsonObject;

		public CachedJsonObject(boolean isCachedResponse, JSONObject jsonObject) {
			super();
			this.isCachedResponse = isCachedResponse;
			this.jsonObject = jsonObject;
		}

		public boolean isCachedResponse() {
			return this.isCachedResponse;
		}

		public void setCachedResponse(boolean isCachedResponse) {
			this.isCachedResponse = isCachedResponse;
		}

		public JSONObject getJsonObject() {
			return this.jsonObject;
		}

		public void setJsonObject(JSONObject jsonObject) {
			this.jsonObject = jsonObject;
		}
	}
}
