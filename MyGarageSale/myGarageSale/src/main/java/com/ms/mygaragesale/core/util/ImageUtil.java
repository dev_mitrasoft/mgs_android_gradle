package com.ms.mygaragesale.core.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.media.ExifInterface;

import com.ms.mygaragesale.core.DefaultConf;
import com.ms.mygaragesale.core.Logger;

import java.io.File;
import java.io.FileOutputStream;

public class ImageUtil {

	private static final int IMAGE_COMPRESS_QUALITY = 78;

	public static Bitmap getRoundedShape(int width, int height, Bitmap scaleBitmapImage) {
		// TODO Auto-generated method stub
		int targetWidth = width;
		int targetHeight = height;
		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth, targetHeight, Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2, ((float) targetHeight - 1) / 2, Math.min((float) targetWidth, (float) targetHeight) / 2, Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight()), new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}
		return inSampleSize;
	}

	public static String compressImage(String imageUri) {
		String outputUri = null;
		try {
			File inFile = new File(imageUri);
//			Log.d("MGS:ImageUtil", "image input disk size: "+ inFile.length());
			Bitmap scaledBitmap = null;
			BitmapFactory.Options options = new BitmapFactory.Options();
	
			// by setting this field as true, the actual bitmap pixels are not
			// loaded in the memory. Just the bounds are loaded. If
			// you try the use the bitmap here, you will get null.
			options.inJustDecodeBounds = true;
			Bitmap inBitmap = BitmapFactory.decodeFile(imageUri, options);
			
			int actualHeight = options.outHeight;
			int actualWidth = options.outWidth;
	
			float maxHeight = DefaultConf.IMAGE_MAX_HEIGHT;
			float maxWidth = DefaultConf.IMAGE_MAX_WIDTH;
			float imgRatio = actualWidth / actualHeight;
			float maxRatio = maxWidth / maxHeight;
	
			// width and height values are set maintaining the aspect ratio of the image
			if (actualHeight > maxHeight || actualWidth > maxWidth) {
				if (imgRatio < maxRatio) {
					imgRatio = maxHeight / actualHeight;
					actualWidth = (int) (imgRatio * actualWidth);
					actualHeight = (int) maxHeight;
				} else if (imgRatio > maxRatio) {
					imgRatio = maxWidth / actualWidth;
					actualHeight = (int) (imgRatio * actualHeight);
					actualWidth = (int) maxWidth;
				} else {
					actualHeight = (int) maxHeight;
					actualWidth = (int) maxWidth;
				}
			}
	
			// setting inSampleSize value allows to load a scaled down version of the original image
			options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
//			Log.d("MGS:ImageUtil", "image inSample size: "+ options.inSampleSize);
	
			// inJustDecodeBounds set to false to load the actual bitmap
			options.inJustDecodeBounds = false;
	
			// this options allow android to claim the bitmap memory if it runs low on memory
			options.inPurgeable = true;
			options.inInputShareable = true;
			options.inTempStorage = new byte[16 * 1024];
	
			// load the bitmap from its path
			inBitmap = BitmapFactory.decodeFile(imageUri, options);
			scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
	
			float ratioX = actualWidth / (float) options.outWidth;
			float ratioY = actualHeight / (float) options.outHeight;
			float middleX = actualWidth / 2.0f;
			float middleY = actualHeight / 2.0f;
	
			Matrix scaleMatrix = new Matrix();
			scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
	
			Canvas canvas = new Canvas(scaledBitmap);
			canvas.setMatrix(scaleMatrix);
			canvas.drawBitmap(inBitmap, middleX - inBitmap.getWidth() / 2, middleY - inBitmap.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
	
			// check the rotation of the image and display it properly
			ExifInterface exif = new ExifInterface(imageUri);
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
			Matrix matrix = new Matrix();
			if (orientation == 6) {
				matrix.postRotate(90);
			} else if (orientation == 3) {
				matrix.postRotate(180);
			} else if (orientation == 8) {
				matrix.postRotate(270);
			}
			scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
	
			FileOutputStream outStream = null;
			File file = new File(imageUri);
			outStream = new FileOutputStream(file);
//			Log.d("MGS:ImageUtil", "image bitmap size: "+ scaledBitmap.getAllocationByteCount());
			// write the compressed bitmap at the destination specified by filename.
			scaledBitmap.compress(Bitmap.CompressFormat.JPEG, IMAGE_COMPRESS_QUALITY, outStream);
			outputUri = file.getAbsolutePath();
			scaledBitmap.recycle();
//			Log.d("MGS:ImageUtil", "image out disk size: "+ file.length());
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		} catch (OutOfMemoryError e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}

		return outputUri;
	}
}
