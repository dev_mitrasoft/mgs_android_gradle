package com.ms.mygaragesale.model;

import com.ms.mygaragesale.core.db.dao.AlertKeywordTable;

import java.io.Serializable;

public class AlertKeyword implements BaseItem, Serializable {

    public static final int ALERT_KEYWORD_TYPE_TAG = 1;
    public static final int ALERT_KEYWORD_TYPE_SEARCH = 2;

    private String keywordName;
    private int keywordType;

    public AlertKeyword() {
    }

    public AlertKeyword(AlertKeywordTable alertKeywordTable) {
        this.keywordName = alertKeywordTable.getKeywordName();
        this.keywordType = alertKeywordTable.getKeywordType();
    }

    public AlertKeywordTable getAlertKeywordTable() {
        AlertKeywordTable alertKeywordTable = new AlertKeywordTable();
        alertKeywordTable.setKeywordName(this.getKeywordName());
        alertKeywordTable.setKeywordType(this.getKeywordType());
        return alertKeywordTable;
    }

    public String getKeywordName() {
        return keywordName;
    }

    public void setKeywordName(String alertKeyword) {
        this.keywordName = alertKeyword;
    }

    public int getKeywordType() {
        return keywordType;
    }

    public void setKeywordType(int keywordType) {
        this.keywordType = keywordType;
    }

    @Override
    public int hashCode() {
        if (keywordName != null) {
            return keywordName.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return this.keywordName.equals(((AlertKeyword) o).keywordName);
    }
}
