package com.ms.mygaragesale.ui;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Constants;

public class MGSActivity extends ActionBarActivity {

	private CloseActivityBroadcastReceiver closeActivityBroadcastReceiver;
	protected Fragment fragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		closeActivityBroadcastReceiver = new CloseActivityBroadcastReceiver();
		IntentFilter intentFilter = new IntentFilter(Constants.CLOSE_ALL_ACTIVITY_LOGOUT_ACTION);
		intentFilter.addAction(Constants.CLOSE_ALL_ACTIVITY_ACCOUNT_BLOCKED_ACTION);
		registerReceiver(closeActivityBroadcastReceiver, intentFilter);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (closeActivityBroadcastReceiver != null) {
			unregisterReceiver(closeActivityBroadcastReceiver);
		}
	}

	@Override
	public void onBackPressed() {
		if (fragment != null && fragment instanceof IActivityEventCallback) {
			if (((IActivityEventCallback) fragment).onActivityBackPressed() == false) {
				super.onBackPressed();
			}
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Fragment fragment = getFragmentManager().findFragmentById(R.id.layout_container);
		if (fragment != null) {
			fragment.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	private class CloseActivityBroadcastReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Constants.CLOSE_ALL_ACTIVITY_LOGOUT_ACTION)) {
				finish();
			}
			if (intent.getAction().equals(Constants.CLOSE_ALL_ACTIVITY_ACCOUNT_BLOCKED_ACTION)) {
				// restrict account blocked activity to finish 
				if (context instanceof AccountBlockedActivity == false) {
					finish();
				}
			}
		}
	}
}
