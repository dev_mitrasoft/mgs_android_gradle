package com.ms.mygaragesale.core.util;

public class StringUtil {
	
	public static boolean isNullOrEmpty(String input) {
		return input == null || input.trim().length() == 0;
	}

}
