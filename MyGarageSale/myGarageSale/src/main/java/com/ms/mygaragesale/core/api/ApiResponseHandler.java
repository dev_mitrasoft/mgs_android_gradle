package com.ms.mygaragesale.core.api;

import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;
import com.ms.mygaragesale.core.api.ApiTask.IApiTaskResponseHandler;
import com.ms.mygaragesale.core.util.UnauthorisedUtility;

public class ApiResponseHandler<T> implements IApiTaskResponseHandler<T> {
	
	@Override
	public void onApiSuccess(T response, boolean isCachedResponse) {
		
	}

	@Override
	public void onApiFailure(MGSError error) {
		if (error.getErrorType() == MGSErrorType.USER_ACCOUNT_BLOCKED) {
			UnauthorisedUtility.processUserAccountBlocked();
		}
		if (error.getErrorType() == MGSErrorType.UNAUTHORIZED_ERROR) {
			UnauthorisedUtility.processUnauthentication();
		}
	}

}
