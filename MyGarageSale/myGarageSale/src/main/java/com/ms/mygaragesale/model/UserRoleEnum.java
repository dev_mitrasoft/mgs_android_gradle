package com.ms.mygaragesale.model;

public enum UserRoleEnum {

	USER, MODERATOR, ADMIN

}
