package com.ms.mygaragesale.model.response;

import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.SaleItem;

import java.util.ArrayList;

public class AlertKeywordsResponse extends Response {

	private int totalResults;
	private int limit;
	private int offset;
	private ArrayList<AlertKeyword> alertKeywords;

	public AlertKeywordsResponse() {
	}

	public AlertKeywordsResponse(int totalResults, int limit, int offset, ArrayList<AlertKeyword> alertKeywords) {
		super();
		this.totalResults = totalResults;
		this.limit = limit;
		this.offset = offset;
		this.alertKeywords = alertKeywords;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public ArrayList<AlertKeyword> getAlertKeywords() {
		return alertKeywords;
	}

	public void setAlertKeywords(ArrayList<AlertKeyword> alertKeywords) {
		this.alertKeywords = alertKeywords;
	}
}
