package com.ms.mygaragesale.ui.frag;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.request.SaleItemRequest;
import com.ms.mygaragesale.model.response.IdResponse;
import com.ms.mygaragesale.model.response.ImagesResponse;
//import com.ms.mygaragesale.model.SaleItemImage;

public class AddNewSaleItemDialogFragment extends LoadingMessageDialogFragment {

	private SaleItem saleItem;
	private boolean isUploadSuccess;
	

	public static AddNewSaleItemDialogFragment newInstance(SaleItem saleItem) {
		AddNewSaleItemDialogFragment addNewSaleItemDialogFragment = new AddNewSaleItemDialogFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable("KEY_SALE_ITEM_OBJECT", saleItem);
		addNewSaleItemDialogFragment.setArguments(bundle);
		return addNewSaleItemDialogFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle bundle = getArguments();
		this.saleItem = (SaleItem) bundle.getSerializable("KEY_SALE_ITEM_OBJECT");
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		if (isUploadSuccess) {
			addSaleItem();
		} else {
			uploadImage();
		}
	}
	
	@Override
	protected void initView() {
		super.initView();
		if (!isProcessStarted) {
			this.isProcessStarted = true;
			this.createNewSaleItem();
		}
	}

	private void createNewSaleItem() {
		// upload images only if there are images inside sale item
		if (this.saleItem.getImages() != null && !this.saleItem.getImages().isEmpty()) {
			uploadImage();
		} else {
            isUploadSuccess = true;
			addSaleItem();
		}
		
//		DatabaseProvider databaseProvider = DatabaseProvider.getInstance();
//		databaseProvider.insertSaleItemImages(saleItem.getImagesList());
//		databaseProvider.insertSaleItem(saleItem);
	}

	private void uploadImage() {
		loadingView.setLoadingMesage("Uploading images..");
		
		ArrayList<String> images = this.saleItem.getImages();
		ApiTask<ImagesResponse> apiTask = new ApiTaskFactory<ImagesResponse>()
				.newImageUploadInstance(getActivity(),
						UrlUtil.getAddImageUrl(), APIMethod.POST,
						ImagesResponse.class, new UploadImageResponseHandler());
		File[] files = new File[images.size()];
		for (int i = 0; i < images.size(); i++) {
			String path = images.get(i);
			files[i] = new File(path);
		}
		apiTask.uploadFileRequest(files);
	}

	private class UploadImageResponseHandler extends
			ApiResponseHandler<ImagesResponse> {

		@Override
		public void onApiSuccess(ImagesResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			isUploadSuccess = true;
			IdResponse[] idResponses = response.getImages();

			ArrayList<String> images = saleItem.getImages();
            if (images != null && idResponses != null) {
                for (int i = 0; i < Math.min(idResponses.length, images.size()); i++) {
                    IdResponse idResponse = idResponses[i];
                    if (idResponse.getErrorCode() > 0) {
                        // TODO error, display to user
                        String saleItemImagePath = images.get(i);
                        if (saleItemImagePath != null) {
                            File file = new File(saleItemImagePath);
                            file.delete();
                            saleItemImagePath = null;
                        }
                    } else {
                        String saleItemImagePath = images.get(i);
                        if (saleItemImagePath != null) {
                            File file = new File(saleItemImagePath);
                            file.delete();
                        }
                        saleItemImagePath = idResponse.getId();
                        images.set(i, saleItemImagePath);
                    }
                }
            }
			addSaleItem();
		}

		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			if (!isDialogDismissed) {
				messageView.showMessage(error);
				showView(ContentViewType.MESSAGE);
			} else {
				MGSApplication.showToast(error.getMesssage(),
						Toast.LENGTH_LONG);
				AddNewSaleItemDialogFragment.this.dismiss();
				if (dialogFragmentCompleteListener != null) {
					dialogFragmentCompleteListener.onComplete(error);
				}
			}
		}
	}
	
	private void addSaleItem() {
		String createMsg = "Creating new sale item..";
		if (saleItem.getSponsored() == SaleItem.SALE_ITEM_SPONSORED) {
			createMsg = "Creating new Ad Post..";
		}
		loadingView.setLoadingMesage(createMsg);
		
		ApiTask<SaleItem> apiTask = new ApiTaskFactory<SaleItem>()
				.newInstance(getActivity(), UrlUtil.getAddNewSaleItem(),
						APIMethod.POST, SaleItem.class,
						new AddNewSaleItemResponseHandler());
		SaleItemRequest saleItemRequest = new SaleItemRequest(saleItem);
		apiTask.executeRequest(saleItemRequest, false);
	}

	private class AddNewSaleItemResponseHandler extends
			ApiResponseHandler<SaleItem> {

		@Override
		public void onApiSuccess(SaleItem saleItem, boolean isCachedResponse) {
			super.onApiSuccess(saleItem, isCachedResponse);
//			DatabaseProvider dbProvider = DatabaseProvider.getInstance();
//			dbProvider.updateSaleItem(saleItem);
			String toastMsg = "New sale item created successfully.";
			if (saleItem.getSponsored() == SaleItem.SALE_ITEM_SPONSORED) {
				toastMsg = "Sponsored Ad Post created successfully.";
			}
			MGSApplication.showToast(toastMsg, Toast.LENGTH_LONG);
			AddNewSaleItemDialogFragment.this.dismiss();
			if (dialogFragmentCompleteListener != null) {
				dialogFragmentCompleteListener.onComplete(null);
			}
			if (CurrentUser.getCurrentUser().getAccountType() == User.ACCOUNT_TYPE_FACEBOOK) {
				//publishStory();
//				postToFacebook();
			}
		}

		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			if (!isDialogDismissed) {
				messageView.showMessage(error);
				showView(ContentViewType.MESSAGE);
			} else {
				MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
				AddNewSaleItemDialogFragment.this.dismiss();
				if (dialogFragmentCompleteListener != null) {
					dialogFragmentCompleteListener.onComplete(error);
				}
			}
		}
	}
	
	public void postToFacebook() {
		Logger.log("publish post");
	    Session session = Session.getActiveSession();
	    if (session == null) {
	    	session = Session.openActiveSessionFromCache(MGSApplication.getInstance().getApplicationContext());
	    }
	    if (session != null) {
	    	Bundle book = new Bundle();
	    	if (saleItem.getImages() != null && !saleItem.getImages().isEmpty()) {
	    		String img = saleItem.getImages().get(0);
				book.putString("picture", UrlUtil.getImageUrl(img));	
			}
	    				
			book.putString("name", saleItem.getTitle() + " in $" + saleItem.getPrice());			
//			book.putString("link", "http://appthisup.com");
			book.putString("description", saleItem.getDescription());
			book.putString("caption", "mygaragesale");
			
			Request.Callback imageCallback = new Request.Callback() {
				@Override
				public void onCompleted(Response response) {
					// Log any response error
					Logger.log("facebook post complete");
					FacebookRequestError error = response.getError();
					if (error != null) {
						Logger.log("facebook post error");
						Logger.log(error.getErrorMessage());
					}
				}
		    };
		    
		    // Create the request for the image upload
//			Request imageRequest = new Request(session, "me/feed", book, HttpMethod.POST, imageCallback);
//	    	imageRequest.executeAsync();
//	    	Request imageRequestGrp = new Request(session, UrlUtil.getFBPostToGroupPathComponent(), book, HttpMethod.POST, imageCallback);
//	    	imageRequestGrp.executeAsync();
	    }
	}
	
	public static void publishStory() {
		// Un-comment the line below to turn on debugging of requests
		//Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);
		Logger.log("publish story");
	    Session session = Session.getActiveSession();
	    if (session == null) {
	    	session = Session.openActiveSessionFromCache(MGSApplication.getInstance().getApplicationContext());
	    }
	    if (session != null) {
	    
//	    	Bundle book = new Bundle();
//				book.putString("picture", 
//						"https://furious-mist-4378.herokuapp.com/books/a_game_of_thrones.png");				
//			book.putString("name", "An item");			
////			book.putString("link",
////					"https://furious-mist-4378.herokuapp.com/books/a_game_of_thrones/");
//			book.putString("description", 
//					"description");
//			book.putString("caption", 
//					"$ 30");
//			
//			Request.Callback imageCallback = new Request.Callback() {
//				@Override
//				public void onCompleted(Response response) {
//					// Log any response error
//					FacebookRequestError error = response.getError();
//					if (error != null) {
//						Logger.log(error.getErrorMessage());
//					}
//				}
//		    };
//								    
//		    // Create the request for the image upload
//			Request imageRequest = new Request(session, 
//					"me/feed", book, HttpMethod.POST, imageCallback);
//	    	imageRequest.executeAsync();
////		    // Check for publish permissions    
////		    List<String> permissions = session.getPermissions();
////		    if (!isSubsetOf(PERMISSIONS, permissions)) {
////		    	pendingPublishReauthorization = true;
////		    	Session.NewPermissionsRequest newPermissionsRequest = new Session 
////		    	.NewPermissionsRequest(this, PERMISSIONS);
////		    	session.requestNewPublishPermissions(newPermissionsRequest);
////		    	return;
////		    }
//		    
		    // Show a progress dialog because the batch request could take a while.
	        
	        
		    try {
				// Create a batch request, firstly to post a new object and
				// secondly to publish the action with the new object's id.
				RequestBatch requestBatch = new RequestBatch();
				
				// Request: Staging image upload request
				// --------------------------------------------
				
				// If uploading an image, set up the first batch request
				// to do this.
//				if (false) {
//					// Set up image upload request parameters
//					Bundle imageParams = new Bundle();
//					Bitmap image = BitmapFactory.decodeResource(this.getResources(), 
//							R.drawable.a_game_of_thrones);
//					imageParams.putParcelable("file", image);
//					
//					// Set up the image upload request callback
//				    Request.Callback imageCallback = new Request.Callback() {
//
//						@Override
//						public void onCompleted(Response response) {
//							// Log any response error
//							FacebookRequestError error = response.getError();
//							if (error != null) {
//								Logger.log(error.getErrorMessage());
//							}
//						}
//				    };
//				    
//				    // Create the request for the image upload
//					Request imageRequest = new Request(Session.getActiveSession(), 
//							"me/staging_resources", imageParams, 
//			                HttpMethod.POST, imageCallback);
//					
//					// Set the batch name so you can refer to the result
//					// in the follow-on object creation request
//					imageRequest.setBatchEntryName("imageUpload");
//					
//					// Add the request to the batch
//					requestBatch.add(imageRequest);
//				}
								
				// Request: Object request
				// --------------------------------------------
				
		    	// Set up the JSON representing the book
				JSONObject book = new JSONObject();
				
				// Set up the book image
				if (false) {
					// Set the book's image from the "uri" result from 
					// the previous batch request
					book.put("image", "{result=imageUpload:$.uri}");
				} else {
					// Set the book's image from a URL
					book.put("image", 
							"https://furious-mist-4378.herokuapp.com/books/a_game_of_thrones.png");
				}				
				book.put("title", "An item");			
				book.put("url",
						"https://furious-mist-4378.herokuapp.com/books/a_game_of_thrones/");
				book.put("description", 
						"description");
//				book.put("caption", 
//						"$ 30");
				JSONObject data = new JSONObject();
				data.put("sale_item_title", "sale_item_title");
				book.put("data", data);
				
				// Set up object request parameters
				Bundle objectParams = new Bundle();
				objectParams.putString("object", book.toString());
				// Set up the object request callback
			    Request.Callback objectCallback = new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						// Log any response error
						Logger.log("upload story complete");
						FacebookRequestError error = response.getError();
						if (error != null) {
							Logger.log(error.getErrorMessage());
						}
					}
			    };
			    
			    // Create the request for object creation
				Request objectRequest = new Request(Session.getActiveSession(), 
						"me/objects/my_garage_sales:sale_item", objectParams, 
		                HttpMethod.POST, objectCallback);
				
				// Set the batch name so you can refer to the result
				// in the follow-on publish action request
				objectRequest.setBatchEntryName("objectCreate");
				
				// Add the request to the batch
				requestBatch.add(objectRequest);
				
				// Request: Publish action request
				// --------------------------------------------
				Bundle actionParams = new Bundle();
				// Refer to the "id" in the result from the previous batch request
				
				actionParams.putString("sale_item", "404930319681785");
				//actionParams.putString("sale_item", "{result=objectCreate:$.id}");
//				actionParams.putString("my_garage_sales.title", "new title");
//				actionParams.putString("my_garage_sales.sale_item_title", "new title");
//				actionParams.putString("sale_item.sale_item_id", "67890");
//				actionParams.putString("sale_item.sale_item_desc", "description");
//				actionParams.putString("sale_item.sale_item_price", "price");
				// Turn on the explicit share flag
				//actionParams.putString("fb:explicitly_shared", "true");
				
				// Set up the action request callback
				Request.Callback actionCallback = new Request.Callback() {

					@Override
					public void onCompleted(Response response) {
						FacebookRequestError error = response.getError();
						Logger.log("publish story complete");
						if (error != null) {
							Logger.log(error.getErrorMessage());
						} else {
							String actionId = null;
							try {
								JSONObject graphResponse = response
				                .getGraphObject()
				                .getInnerJSONObject();
								actionId = graphResponse.getString("id");
							} catch (Exception e) {
								Logger.log("JSON error "+ e.getMessage());
							}
							Logger.log(actionId);
						}
					}
				};
				
				// Create the publish action request
				Request actionRequest = new Request(Session.getActiveSession(),
						"me/my-garage-sales:sale", actionParams, HttpMethod.POST,
						actionCallback);
				
				// Add the request to the batch
				requestBatch.add(actionRequest);
				
				// Execute the batch request
				requestBatch.executeAsync();
				Logger.log("publish story start");
			} catch (JSONException e) {
				Logger.log(e.getMessage());
				
			}
		}
	}

}
