package com.ms.mygaragesale.push.message.model;

import com.google.gson.annotations.SerializedName;

public class NewSaleItemAdded {

    @SerializedName("itemId")
    private String itemId;

    @SerializedName("sellerName")
    private String sellerName;

    @SerializedName("itemTitle")
    private String itemTitle;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }
}
