package com.ms.mygaragesale.ui.view;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.request.AlertKeywordsRequest;
import com.ms.mygaragesale.model.response.SuccessResponse;

public class SearchBarView extends RelativeLayout {

	private static final int VIEW_SWITCHER_ALERT_INDEX = 0;
	private static final int VIEW_SWITCHER_PROGRESS_INDEX = 1;

	private AutoCompleteTextViewEx autoCompleteTextView;
	private ImageButton clearButton;
	private ImageButton alertButton;
	private ViewSwitcher viewSwitcherAlert;
	private ArrayList<String> searchSuggesstions;
	private ArrayList<AlertKeyword> alertKeywordsList;

	private boolean isKeywordAlreadyAddedForAlerts;
	private SearchListener searchListener;

	public SearchBarView(Context context)
	{
		super(context);
		this.init();
	}

	public SearchBarView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.init();
	}

	public SearchBarView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		this.init();
	}

	public void setSearchListener(SearchListener listener)
	{
		this.searchListener = listener;
	}

	/**
	 * 	Method to set focus to SearchBar and update its UI.
	 */
	public Boolean setFocus()
	{
		if (!this.autoCompleteTextView.hasFocus())
		{
			this.autoCompleteTextView.requestFocus();

			this.autoCompleteTextView.post(new Runnable() {
				@Override
				public void run() {
					final InputMethodManager imm = (InputMethodManager) SearchBarView.this.getContext()
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(SearchBarView.this.autoCompleteTextView, 0);
				}
			});
			return true;
		}
		return false;
	}

	public Boolean hideKeypad() {
		final InputMethodManager imm = (InputMethodManager) this.getContext()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		final Boolean res = imm.hideSoftInputFromWindow(this.autoCompleteTextView.getWindowToken(), 0);
		return res;
	}

	private void init() {
		View.inflate(this.getContext(), R.layout.view_search_bar, this);

		this.configureAutoCompleteTextView();

		this.clearButton = (ImageButton) this.findViewById(R.id.cancel_button);
		this.viewSwitcherAlert = (ViewSwitcher) this.findViewById(R.id.view_switcher_alert);
		this.alertButton = (ImageButton) this.findViewById(R.id.image_button_alert);

		this.clearButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SearchBarView.this.autoCompleteTextView.setText("");
				SearchBarView.this.viewSwitcherAlert.setVisibility(View.INVISIBLE);
				SearchBarView.this.clearButton.setVisibility(View.INVISIBLE);
			}
		});
		this.alertButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				updateKeywordForAlerts();
			}
		});

		// get alert keywords list to add/remove keyword for alerts.
		alertKeywordsList = DatabaseProvider.getInstance().getAlertKeywords();
	}

	private void performSearchForText(String keyword) {
		if (!StringUtil.isNullOrEmpty(keyword)) {
			if (!searchSuggesstions.contains(keyword)) {
				searchSuggesstions.add(keyword);
				Collections.sort(searchSuggesstions);
				((AutoCompleteTextAdapter) autoCompleteTextView.getAdapter()).notifyDataSetChanged();
			}
			this.autoCompleteTextView.dismissDropDown();
			hideKeypad();
			if (this.searchListener != null) {
				this.searchListener.searchText(keyword);
			}
		}
	}

	/**
	 * 	Configures AutoCompleteTextView with adpater and search action.
	 */
	private void configureAutoCompleteTextView()
	{
		this.autoCompleteTextView = (AutoCompleteTextViewEx) this.findViewById(R.id.searchText);
		this.autoCompleteTextView.setThreshold(0);
		this.autoCompleteTextView.setDropDownVerticalOffset(SizeUtil.getSizeInPixels(1));
		this.autoCompleteTextView.setDropDownHorizontalOffset(SizeUtil.getSizeInPixels(-56));
		this.autoCompleteTextView.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				SearchBarView.this.hideKeypad();
			}
			}
		});

		try {
			final Field field = AutoCompleteTextView.class.getDeclaredField("mPopup");
			field.setAccessible(true);
			final ListPopupWindow popupWindow = (ListPopupWindow) field.get(this.autoCompleteTextView);
			popupWindow.setAnimationStyle(0);
		} catch (final NoSuchFieldException e) {
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			e.printStackTrace();
		}

		this.autoCompleteTextView.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				if (s.length() == 0) {
					SearchBarView.this.viewSwitcherAlert.setVisibility(View.INVISIBLE);
					SearchBarView.this.viewSwitcherAlert.setDisplayedChild(VIEW_SWITCHER_ALERT_INDEX);
					SearchBarView.this.alertButton.setImageResource(R.drawable.ic_visibility_black_24dp);
					SearchBarView.this.clearButton.setVisibility(View.INVISIBLE);
				} else {
					SearchBarView.this.viewSwitcherAlert.setVisibility(View.VISIBLE);
					SearchBarView.this.clearButton.setVisibility(View.VISIBLE);

					// update visibility icon to add/remove keyword for alerts
					String text = s.toString();
					isKeywordAlreadyAddedForAlerts = true;
					if (alertKeywordsList != null) {
						for (AlertKeyword alertKeyword : alertKeywordsList) {
							if (text.equals(alertKeyword.getKeywordName())) {
								isKeywordAlreadyAddedForAlerts = false;
								break;
							}
						}
					}
					if (isKeywordAlreadyAddedForAlerts) {
						SearchBarView.this.alertButton.setImageResource(R.drawable.ic_visibility_black_24dp);
					} else {
						SearchBarView.this.alertButton.setImageResource(R.drawable.ic_visibility_off_black_24dp);
					}
				}
			}
		});

		this.autoCompleteTextView.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEARCH) {
					SearchBarView.this.performSearchForText(SearchBarView.this.autoCompleteTextView.getText()
							.toString());
					return true;
				}
				return false;
			}
		});

		this.autoCompleteTextView.setAnimation(null);

		this.autoCompleteTextView.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				try {
					SearchBarView.this.autoCompleteTextView.showDropDown();
				} catch(final Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		});

		searchSuggesstions = DatabaseProvider.getInstance().getSearchedKeywords();
		Collections.sort(searchSuggesstions);
		this.autoCompleteTextView.setAdapter(new AutoCompleteTextAdapter(searchSuggesstions));

		this.autoCompleteTextView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final String selectedText = (String) view.getTag();
				SearchBarView.this.performSearchForText(selectedText);
			}
		});

	}

	private void updateKeywordForAlerts() {
		String url = null;
		final String text = autoCompleteTextView.getText().toString();
		final boolean addNewKeword = isKeywordAlreadyAddedForAlerts;
		if (addNewKeword) {
			url = UrlUtil.getAddAlertKeywordsUrl();
		} else {
			url = UrlUtil.getDeleteAlertKeywordsUrl();
		}
		final ArrayList<AlertKeyword> alertKeywords = new ArrayList<>(1);
		AlertKeyword alertKeyword = new AlertKeyword();
		alertKeyword.setKeywordName(text);
		alertKeyword.setKeywordType(AlertKeyword.ALERT_KEYWORD_TYPE_SEARCH);
		alertKeywords.add(alertKeyword);

		ApiTask<SuccessResponse> apiTask = new ApiTaskFactory<SuccessResponse>().newInstance(getContext(), url, ApiTask.APIMethod.POST, SuccessResponse.class, new ApiResponseHandler<SuccessResponse>() {

			@Override
			public void onApiFailure(MGSError error) {
				super.onApiFailure(error);
				String toastMessage = null;
				if (error.getErrorType() == MGSError.MGSErrorType.NO_NETWORK_CONNECTION) {
					if (addNewKeword) {
						toastMessage = "No Internet Connection. Unable to add keyword(s) for item alerts.";
					} else {
						toastMessage = "No Internet Connection. Unable to remove keyword(s) for item alerts.";
					}
				} else {
					toastMessage = error.getMesssage();
				}
				MGSApplication.showToast(toastMessage, Toast.LENGTH_LONG);
				viewSwitcherAlert.setDisplayedChild(VIEW_SWITCHER_ALERT_INDEX);
			}

			@Override
			public void onApiSuccess(SuccessResponse response, boolean isCachedResponse) {
				super.onApiSuccess(response, isCachedResponse);
				if (addNewKeword) {
					DatabaseProvider.getInstance().addAlertKeywords(alertKeywords);
					alertKeywordsList.addAll(alertKeywords);
					MGSApplication.showToast("Keyword added in watch list.", Toast.LENGTH_LONG);
					alertButton.setImageResource(R.drawable.ic_visibility_off_black_24dp);
				} else {
					DatabaseProvider.getInstance().deleteAlertKeywords(alertKeywords);
					alertKeywordsList.removeAll(alertKeywords);
					MGSApplication.showToast("Keyword removed from watch list.", Toast.LENGTH_LONG);
					alertButton.setImageResource(R.drawable.ic_visibility_black_24dp);
				}
				viewSwitcherAlert.setDisplayedChild(VIEW_SWITCHER_ALERT_INDEX);
			}
		});
		AlertKeywordsRequest alertKeywordsRequest = new AlertKeywordsRequest();
		alertKeywordsRequest.setAlertKeywords(alertKeywords);
		apiTask.executeRequest(alertKeywordsRequest, false);
		viewSwitcherAlert.setDisplayedChild(VIEW_SWITCHER_PROGRESS_INDEX);
	}

	/**
	 * Adapter for AutoComplete Text view.
	 *
	 */
	private class AutoCompleteTextAdapter extends BaseAdapter implements Filterable
	{
		private List<String> suggestions;
		private Integer searchedKeywordCount;
		
		public AutoCompleteTextAdapter(List<String> objects) {
			super();
			suggestions = objects;
		}

		@Override
	    public int getCount() {
	        return suggestions.size();
	    }
		
	    @Override
	    public String getItem(int position) {
	        return suggestions.get(position);
	    }

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = View.inflate(SearchBarView.this.getContext(), R.layout.view_search_suggestions, null);
			}
			final TextView textView = (TextView) view.findViewById(R.id.autocomplete_listitem_text);
			ImageView imageView = (ImageView) view.findViewById(R.id.image_view_searched_icon);
			View bottomBar = view.findViewById(R.id.view_bottom_bar);
			
			imageView.setImageResource(R.drawable.ic_history_grey600_18dp);
			final String text = this.getItem(position);
			textView.setText(text);
			view.setTag(text);
			
			if (position == getCount() - 1) {
				bottomBar.setVisibility(View.VISIBLE);
			} else {
				bottomBar.setVisibility(View.GONE);
			}
						
			return view;
		}
		
		@Override
		public Filter getFilter() {
		    return new Filter() {
		        @Override
		        protected void publishResults(CharSequence constraint, FilterResults results) {
		            if (results.count > 0) {
		            	suggestions = (List<String>) results.values;
		                notifyDataSetChanged();
		            } else {
		                notifyDataSetInvalidated();
		            }
		        }

		        @Override
		        protected FilterResults performFiltering(CharSequence constraint) {
		        	// adding search history keywords
		            ArrayList<String> result = new ArrayList<>();
		            if (StringUtil.isNullOrEmpty(constraint.toString())) {
						result.addAll(searchSuggesstions);
					} else {
			            for (String string : searchSuggesstions) {
			            	if (string.startsWith(constraint.toString())) {
								result.add(string);
							}
						}
					}
		            
		            searchedKeywordCount = Integer.valueOf(result.size());
		            FilterResults filterResult = new FilterResults();
		            filterResult.values = result;
		            filterResult.count = result.size();
		            return filterResult;
		        }
		    };
		}
	}
	
	public interface SearchListener 
	{
		public void searchText(String text);
	}

}
