package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.facebook.widget.LoginButton;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.util.UnauthorisedUtility;
import com.ms.mygaragesale.model.CurrentUser;

/**
 * Facebook login button is extended to get touch event on facebook button.
 * When we get touch event previous session of facebook is terminated.
 */
public class FbLoginButtonEx extends LoginButton {

    public FbLoginButtonEx(Context context) {
        super(context);
    }

    public FbLoginButtonEx(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FbLoginButtonEx(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            UnauthorisedUtility.terminateFacebookSession(CurrentUser.getCurrentUser());
        }
        return super.onTouchEvent(event);
    }

}
