package com.ms.mygaragesale.model.google;

import com.ms.mygaragesale.model.response.Response;

public class GPlaceDetail extends Response {

	private GPlaceDetailResult result;
	
	public GPlaceDetailResult getResult() {
		return result; 
	}
	
	public void setResult(GPlaceDetailResult result) {
		this.result = result;
	}
	
	public GPlaceDetailResult getPlaceDetailResult() {
		return getResult();
	}
}
