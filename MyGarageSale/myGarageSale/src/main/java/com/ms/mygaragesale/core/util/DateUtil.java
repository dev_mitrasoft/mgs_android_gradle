package com.ms.mygaragesale.core.util;

import android.util.Log;

import com.ms.mygaragesale.core.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	
	//Array of date formats returned by different SAVO web apis.
    private static final String[] SUPPORTED_DATE_FORMATS = {"MM/dd/yyyy h:m:s a", //recently viewed, search, questions
    														"E MMM dd hh:mm:ss z yyyy"}; //recommended kits
    														//"dd/MM/yyyy"}; //quick start and recommended kits update timestamp in shared preferences
    private static final String TIMEZONE_UTC = "UTC";
	
	public static String getDateOnlyString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		return simpleDateFormat.format(date);
	}
	
	public static String getDateTimeOnlyString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		return simpleDateFormat.format(date);
	}
	
	public static String getLocalTimeAndDate(long milliSeconds)
	{
		Date utcInputDate = new Date(milliSeconds);
		String message = null;

        if(utcInputDate != null) {
    		Calendar utcInputCalendar = Calendar.getInstance();
    		utcInputCalendar.setTime(utcInputDate);
    		Calendar utcTodayCalendar = Calendar.getInstance();

            if (utcInputCalendar.after(utcTodayCalendar)) {
                return "0m";
            }


            int year = utcTodayCalendar.get(Calendar.YEAR) - utcInputCalendar.get(Calendar.YEAR);
    		int numDaysInPrevYear = 0; 
    		if (year > 0) {
				numDaysInPrevYear = utcInputCalendar.getActualMaximum(Calendar.DAY_OF_YEAR);
			}
    		int day = numDaysInPrevYear + utcTodayCalendar.get(Calendar.DAY_OF_YEAR) - utcInputCalendar.get(Calendar.DAY_OF_YEAR);
    		int hour = utcTodayCalendar.get(Calendar.HOUR_OF_DAY) - utcInputCalendar.get(Calendar.HOUR_OF_DAY);
    		int minutes = utcTodayCalendar.get(Calendar.MINUTE) - utcInputCalendar.get(Calendar.MINUTE);
    		
    		if(day > 0) {
    			return (day + "d");
    		} else if(hour > 0 && hour < 24) {
    			return (hour+"h");
    		} else if(minutes >= 0 && minutes < 59) {
    			return (minutes + "m");
			}
        }
		return message;
	}
	
	public static String getDateTimeText(Date date)
	{
		String message = null;

        if(date != null) {
    		Calendar utcInputCalendar = Calendar.getInstance();
    		utcInputCalendar.setTime(date);
    		Calendar utcTodayCalendar = Calendar.getInstance();

            if (utcInputCalendar.after(utcTodayCalendar)) {
                return "Just now";
            }
		    
    		int year = utcTodayCalendar.get(Calendar.YEAR) - utcInputCalendar.get(Calendar.YEAR);
    		int monthsInPrevYear = 0; 
    		if (year > 0) {
    			monthsInPrevYear = utcInputCalendar.getActualMaximum(Calendar.MONTH);
			}
    		int month = monthsInPrevYear + utcTodayCalendar.get(Calendar.MONTH) - utcInputCalendar.get(Calendar.MONTH);
    		int day = utcTodayCalendar.get(Calendar.DAY_OF_MONTH) - utcInputCalendar.get(Calendar.DAY_OF_MONTH);
    		int hour = utcTodayCalendar.get(Calendar.HOUR_OF_DAY) - utcInputCalendar.get(Calendar.HOUR_OF_DAY);
    		int minutes = utcTodayCalendar.get(Calendar.MINUTE) - utcInputCalendar.get(Calendar.MINUTE);
    		int seconds = utcTodayCalendar.get(Calendar.SECOND) - utcInputCalendar.get(Calendar.SECOND);
    		String lastModifiedString = "";
    		if(month > 0) {
    			if (month == 1) {
    				return (lastModifiedString + month + " month ago");
				} else {
					return (lastModifiedString + month + " months ago");
				}
    		} else if(day > 0) {
    			if (day == 1) {
    				return (lastModifiedString + day + " day ago");
				} else {
					return (lastModifiedString + day + " days ago");
				}
    		} else if(hour > 0 && hour < 24) {
    			if (hour == 1) {
    				return (lastModifiedString + hour +" hour ago");
				} else {
					return (lastModifiedString + hour +" hours ago");
				}
    		} else if(minutes > 0 && minutes < 59) {
    			if (minutes == 1) {
    				return (lastModifiedString + minutes + " minute ago");
				} else {
					return (lastModifiedString + minutes + " minutes ago");
				}
			} else if(seconds >= 0 && seconds < 59) {
				if (seconds <= 0) {
    				return (lastModifiedString + "Just now");
				} else if (seconds == 1) {
    				return (lastModifiedString + seconds + " second ago");
				} else {
					return (lastModifiedString + seconds + " seconds ago");
				}
			}
        }
		return message;
	}
	
	private static Date parseDateTimeString(String utcInputTime) {
		Date utcInputDate = null;
		SimpleDateFormat format = null;
	    //try to parse the input date using all supported date formats 
	    for(int i = 0; i < SUPPORTED_DATE_FORMATS.length; i++) {
		    //convert current utc date from string form to date format.
	        format = new SimpleDateFormat(SUPPORTED_DATE_FORMATS[i]);
	        format.setTimeZone(TimeZone.getTimeZone(TIMEZONE_UTC));
	        try {
	            utcInputDate = format.parse(utcInputTime);
	            break;
	        } catch(ParseException parseEx) {
	            android.util.Log.e("DateUtils", "Exception caught when parsing date. with format - " + SUPPORTED_DATE_FORMATS[i]);
	        }
        }
	    return utcInputDate;
	}

}
