package com.ms.mygaragesale.core.db;


public class DBConst {
	
	public static class TableName {
		public static final String USER = "user";
		public static final String IMAGE = "image";
		public static final String SALE_ITEM = "saleItem";
	}
	
	public static class ColumnName { 
		public static final String ID = "id"; 
		public static final String USERNAME = "username"; 
		public static final String PASSWORD = "password"; 
		public static final String NAME = "name"; 
		public static final String EMAIL_ID = "emailId"; 
		public static final String MOBILE_NUMBER = "mobileNumber"; 
		public static final String USER_ROLE = "userRole"; 
		public static final String ACCOUNT_STATUS = "accountStatus"; 
		public static final String ADDRESS = "address"; 
		public static final String LOCATION = "location"; 
		public static final String ACCOUNT_TYPE = "accountType"; 
		public static final String ACCOUNT_ID = "accountId"; 
		public static final String ACCESS_TOKEN = "accessToken"; 
		public static final String IMAGE_ID = "imageId"; 
		public static final String IMAGE_URI = "imageUri";
		
		public static final String USER_ID = "userId";
		public static final String SALE_ITEM_ID = "saleItemId";
		public static final String TITLE = "title";
		public static final String DESCRIPTION = "description";
		public static final String PRICE = "price";
		public static final String CREATED_DATE = "createdDate";
		public static final String MODIFIED_DATE = "ModifiedDate";
		public static final String IMAGES_ONE = "imageOne";
		public static final String IMAGES_TWO = "imageTwo";
		public static final String IMAGES_THREE = "imageThree";
		public static final String STATUS = "status";
		public static final String SELLER_ID = "sellerId";
		public static final String MESSAGE_ID = "messageId";
		public static final String MESSAGE_COUNT = "messageCount";

	}

}
