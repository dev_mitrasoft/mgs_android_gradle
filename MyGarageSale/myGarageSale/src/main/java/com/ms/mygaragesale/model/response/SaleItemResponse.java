package com.ms.mygaragesale.model.response;

import java.util.Date;

import com.ms.mygaragesale.model.Message;
import com.ms.mygaragesale.model.User;

public class SaleItemResponse extends Response {
	
	private String itemId;
	private String title;
	private String description;
	private long price;
	private Date createdDate;
	private Date modifiedDate;
	private int itemStatus;
	private User seller;
	private Message[] message;
	private int messageCount;
	private String[] images;
	private int sponsored;
	
	public SaleItemResponse() {
		// TODO Auto-generated constructor stub
	}

	public SaleItemResponse(String itemId, String title, String description,
			long price, Date createdDate, Date modifiedDate, int itemStatus,
			User seller, Message[] message, int messageCount, String[] images) {
		super();
		this.itemId = itemId;
		this.title = title;
		this.description = description;
		this.price = price;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.itemStatus = itemStatus;
		this.seller = seller;
		this.message = message;
		this.messageCount = messageCount;
		this.images = images;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(int itemStatus) {
		this.itemStatus = itemStatus;
	}

	public User getSeller() {
		return seller;
	}

	public void setSeller(User seller) {
		this.seller = seller;
	}

    public Message[] getMessage() {
        return message;
    }

    public void setMessage(Message[] message) {
        this.message = message;
    }

    public int getMessageCount() {
		return messageCount;
	}

	public void setMessageCount(int messageCount) {
		this.messageCount = messageCount;
	}

	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}
	public int getSponsored() {
		return sponsored;
	}
	
	public void setSponsored(int sponsored) {
		this.sponsored = sponsored;
	}
	
}
