package com.ms.mygaragesale.model;

public class LocalImage {
	
	private String id;
	private String imageUri;
	
	public LocalImage() {
		// TODO Auto-generated constructor stub
	}

	public LocalImage(String id, String imageUri) {
		super();
		this.id = id;
		this.imageUri = imageUri;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImageUri() {
		return imageUri;
	}

	public void setImageUri(String imageUri) {
		this.imageUri = imageUri;
	}
	
	

}
