package com.ms.mygaragesale.model.google;

import com.ms.mygaragesale.model.Location;

/**
 * Used to store place id in case of Google Place API 
 */
public class GLocation extends Location {
	
	private static final long serialVersionUID = 3495536576158149271L;
	
	public GLocation(GPlace place) {
		super(place.getDescription());
		this.placeId = place.getPlaceId();
	}
	
	private transient String placeId;
	
	public String getPlaceId() {
		return placeId;
	}
	
	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}
	
}
