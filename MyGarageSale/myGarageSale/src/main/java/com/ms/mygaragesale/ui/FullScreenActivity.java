package com.ms.mygaragesale.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;

import com.ms.garagesaledeal.R;

public class FullScreenActivity extends MGSActivity {
	
	public static final String KEY_FULL_SCREEN_ACTIVITY_BUNDLE = "fullscreenActivityBundle";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_fullscreen);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.initialize();
	}
	
	private void initialize() {
		Bundle bundle = getIntent().getBundleExtra(KEY_FULL_SCREEN_ACTIVITY_BUNDLE);
		if (bundle == null) {
			throw new IllegalArgumentException("Intent does not contain any bundle data with key " + KEY_FULL_SCREEN_ACTIVITY_BUNDLE);
		}
		Object object = bundle.getSerializable(ActivityBundleFactory.KEY_FRAGMENT_CLASS_NAME);
		if (object == null) {
			throw new IllegalArgumentException("Bundle does not contains any fragment class name.");
		}
		
		try {
			FragmentManager fragmentManager = getFragmentManager();
			fragment = fragmentManager.findFragmentById(R.id.layout_container);
			if (fragment == null) {
				Class<Fragment> fragmentClass = (Class<Fragment>) object;
				fragment = fragmentClass.newInstance();
				fragment.setArguments(bundle);
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.replace(R.id.layout_container, fragment);
				fragmentTransaction.commit();
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			throw new IllegalArgumentException(ex.getMessage());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			this.finish();
		}
		return super.onOptionsItemSelected(item);
	}

}
