package com.ms.mygaragesale.model.google;

public class GPlaceDetailResult {

	private GGeometry geometry;
	
	public GGeometry getGeometry() {
		return geometry;
	}
	
	public void setGeometry(GGeometry geometry) {
		this.geometry = geometry;
	}
	
}
