package com.ms.mygaragesale.ui.frag;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;
import com.ms.mygaragesale.core.util.AuthUtility;
import com.ms.mygaragesale.core.util.AuthUtility.AuthManagerStatusListener;
import com.ms.mygaragesale.core.util.LocationUtil;
import com.ms.mygaragesale.model.CurrentUser;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class AuthenticationDialogFragment extends LoadingMessageDialogFragment implements AuthManagerStatusListener, LocationUtil.OnLocationUpdateListener {
	
	public static final int DO_LOGIN = 0;
	public static final int DO_SIGNUP = 1;
	public static final int DO_COMPLETE_PROCESS = 2;

	private AuthUtility authManager;
	private int authProcess;
	private boolean isSettingsInvoked;
	private LocationUtil locationUtil;
	private boolean isLocationFetched;
	
	public static AuthenticationDialogFragment newInstance(int doLoginOrSignup) {
		AuthenticationDialogFragment addNewSaleItemDialogFragment = new AuthenticationDialogFragment();
		addNewSaleItemDialogFragment.authProcess = doLoginOrSignup;
		return addNewSaleItemDialogFragment;
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		this.startAuthorization();
	}
	
	@Override
	protected void initView() {
		super.initView();
		if (!isProcessStarted) {
			isProcessStarted = true;
			startAuthorization();
		}
	}

	private void startAuthorization() {
		authManager = AuthUtility.newInstance(this);
		if (authProcess == DO_LOGIN) {
			authManager.processLogin();
		} else if (authProcess == DO_SIGNUP) {
			authManager.processSignup();
		} else {
			authManager.processAuthentication();
		}
	}
	
	@Override
	public void onSuccess() {
		MGSApplication.showToast("Logged In", Toast.LENGTH_LONG);
		if (dialogFragmentCompleteListener != null) {
			dialogFragmentCompleteListener.onComplete(null);
		}
	}
	
	@Override
	public void onFailure(MGSError error) {
		if (!isDialogDismissed) {
			// fetch location if dialog is not dismissed else show error message
			if (error.getErrorType() == MGSErrorType.LOCATION_UNAVAILABLE_ERROR) {
				getLocation();
				return;
			}
			if (error.getErrorType() == MGSErrorType.CONFLICT_ERROR || error.getErrorType() == MGSErrorType.UNAUTHORIZED_ERROR) {
				messageView.showMessage(error, false);
				this.getDialog().setCanceledOnTouchOutside(true);
			} else {
				messageView.showMessage(error);
			}
			showView(ContentViewType.MESSAGE);
		} else {
			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
			AuthenticationDialogFragment.this.dismiss();
			if (dialogFragmentCompleteListener != null) {
				dialogFragmentCompleteListener.onComplete(error);
			}
		}
	}
	
	@Override
	public void onMessage(String message) {
		loadingView.setLoadingMesage(message);
	}

	public void getLocation() {
		this.onMessage("Getting Current Location..");

		// TODO: check if gps is on, make it on
		// fetch location
		// get string representations from coordinates
		LocationManager locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		boolean enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		// check if enabled and if not send user to display a dialog and suggesting to go to the settings
		if (!enabled) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Use Location?");
			builder.setMessage("This app wants to change your device settings: \nUse GPS, Wifi or network for location?");
			builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
					getActivity().startActivity(intent);
					isSettingsInvoked = true;
					isLocationChangerSelected = true;
				}
			});
			builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// if no complete signup process
					isLocationChangerSelected = true;
					resumeProcessSignup();
				}
			});
			AlertDialog dialog = builder.show();
			dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					if (!isLocationChangerSelected) {
						// if no complete signup process
						resumeProcessSignup();
					}
				}
			});
		} else {
			fetchLocationOrSignup();
		}
	}
	boolean isLocationChangerSelected;

	private void fetchLocationOrSignup() {
		// check if gps enabled/network enable
		// start fetching location for one minute, if not get do signup
		locationUtil = new LocationUtil(getActivity());
		android.location.Location location = locationUtil.getLocation();
		updateFetchedLocation(location);
	}

	private void updateFetchedLocation(final Location location) {
		if (location != null) {
			Logger.log(Log.DEBUG, "MGS::AuthDialogFrag", "lat:" + location.getLatitude() + ", lon:" + location.getLongitude());
			Geocoder geocoder = new Geocoder(getActivity(), Locale.CANADA);
			try {
				List<Address> locAdds = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
				Address address = locAdds.get(0);
				StringBuilder strReturnedAddress = new StringBuilder();
				for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
					if (i != 0) {
						strReturnedAddress.append(", ");
					}
					strReturnedAddress.append(address.getAddressLine(i));
				}
				Logger.log(Log.DEBUG, "MGS::AuthDialogFrag", "address: " + strReturnedAddress);
				// update fetched location and continue signup process
				CurrentUser currentUser = CurrentUser.getCurrentUser();
				com.ms.mygaragesale.model.Location loc = new com.ms.mygaragesale.model.Location();
				loc.setLatitude(location.getLatitude());
				loc.setLongitude(location.getLongitude());
				loc.setName(strReturnedAddress.toString());
				currentUser.setLocation(loc);
				isLocationFetched = true;
				resumeProcessSignup();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// if we get location then stop using gps
			locationUtil.stopUsingGPS();
			locationUtil.setOnLocationUpdateListener(null);
		} else {
			LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

			// getting GPS status
			boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			if (isGPSEnabled || isNetworkEnabled) {

				// wait for 30 seconds to get location update, if not found then resume signup
				new Handler(Looper.getMainLooper()) {
					@Override
					public void handleMessage(Message msg) {
						super.handleMessage(msg);
						if (!isLocationFetched) {
							if (locationUtil != null) {
								locationUtil.stopUsingGPS();
								locationUtil.setOnLocationUpdateListener(null);
							}
							resumeProcessSignup();
						}
					}
				}.sendEmptyMessageDelayed(0, 30*1000);
			} else {
				// process signup if neither gps nor network is enabled
				resumeProcessSignup();
			}
		}
	}

	@Override
	public void onLocationUpdate(Location location) {
		updateFetchedLocation(location);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (isSettingsInvoked) {
			fetchLocationOrSignup();
		}
		isSettingsInvoked = false;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (locationUtil != null) {
			locationUtil.setOnLocationUpdateListener(this);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (locationUtil != null) {
			locationUtil.setOnLocationUpdateListener(null);
		}
	}

	/** Method to resume signup progress, also display a toast if location is not detected. */
	private void resumeProcessSignup() {
		CurrentUser currentUser = CurrentUser.getCurrentUser();
		com.ms.mygaragesale.model.Location location = currentUser.getLocation();
		if (location == null || location.getLatitude() <= 0 || location.getLongitude() <= 0 || TextUtils.isEmpty(location.getName())) {
			MGSApplication.showToast("Unable to fetch current location. Please update afterwards in my account.", Toast.LENGTH_SHORT);
		}
		authManager.processSignup();
		Logger.log(Log.DEBUG, "MGS::AuthenticationDialogFragment", "resume sign-up.");
	}
}
