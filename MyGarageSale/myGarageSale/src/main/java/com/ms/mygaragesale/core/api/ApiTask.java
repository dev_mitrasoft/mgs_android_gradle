package com.ms.mygaragesale.core.api;

import java.io.File;

import android.content.Context;

import com.ms.mygaragesale.core.MGSError;

public abstract class ApiTask<T> {
	
	protected Context context;
	protected String url;
	protected APIMethod method;
	protected Class<T> responseObjectClass;
	protected IApiTaskResponseHandler<T> apiTaskResponseHandler;
		
	protected ApiTask(Context context, String url, APIMethod method, Class<T> responseObjectClass, IApiTaskResponseHandler<T> apiTaskResponseHandler) {
		super();
		this.context = context;
		this.url = url;
		this.method = method;
		this.responseObjectClass = responseObjectClass;
		this.apiTaskResponseHandler = apiTaskResponseHandler;
	}

	protected String getTag() {
		return url;
	}
	
	public abstract void executeRequest(Object requestObject, boolean shouldResultCachedResponse);
	
	public abstract void uploadFileRequest(File[] files);
	
	public abstract void cancel();
	
//	public static ApiTask<T> newInstance(Context context, String url, APIMethod method, Class<T> responseObjectClass, IApiTaskResponseHandler<T> apiTaskResponseHandler) {
//		return new VolleyApiTask(context, url, method, responseObjectClass, apiTaskResponseHandler);
//	}
//	
//	public static ApiTask newImageUploadInstance(Context context, String url, APIMethod method, Class<T> responseObjectClass, IApiTaskResponseHandler<T> apiTaskResponseHandler) {
//		return new ImageUploadTask(context, url, method, responseObjectClass, apiTaskResponseHandler);
//	}
	
	public interface IApiTaskResponseHandler<T> {
		void onApiSuccess(T response, boolean isCachedResponse);
		void onApiFailure(MGSError error);
	}
	
	public enum APIMethod {
		POST, GET, PUT, DELETE
	}

}
