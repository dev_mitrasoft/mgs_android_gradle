package com.ms.mygaragesale.model;

import android.net.Uri;

public class PickedImage {
	
	private Uri uri;
	
	public Uri getUri() {
		return uri;
	}
	
	public void setUri(Uri uri) {
		this.uri = uri;
	}

}
