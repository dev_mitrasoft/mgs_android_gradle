package com.ms.mygaragesale.ui;

public interface IActivityEventCallback {

	boolean onActivityBackPressed();
	
}
