package com.ms.mygaragesale.ui;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActionBar.LayoutParams;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.ui.frag.SearchFragment;
import com.ms.mygaragesale.ui.view.SearchBarView;
import com.ms.mygaragesale.ui.view.SearchBarView.SearchListener;

/**
 * This activity is translucent full screen activity and it slides from right side of device.
 * This activity is displayed similar to open navigation drawer.
 */
public class SearchActivity extends MGSActivity implements SearchListener {
	
	private ViewGroup layoutRoot;
	private ViewGroup layoutMain;
	private SearchBarView searchBar;
	
	private ValueAnimator backgroundAlphaAnimator;
	private ObjectAnimator searchBarTranslateAnimation;
	
	// this flag is used to prevent animation of views again when orientation changes.
	private boolean isAnimationStarted;	
		
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		this.setContentView(R.layout.activity_search);
		layoutRoot = (ViewGroup) findViewById(R.id.layout_root_view);
		layoutMain = (ViewGroup) findViewById(R.id.layout_main_view);
		Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar_light);
		this.setSupportActionBar(toolbar);
		this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		this.initialize();
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		this.isAnimationStarted = savedInstanceState.getBoolean("IS_ORIENATION_CHANGED", false);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("IS_ORIENATION_CHANGED", this.isAnimationStarted);
	}
	
	/**
	 * This method is overridden to close activity with animation.  
	 */
	@Override
	public void finish() {
		this.reverseAnimation();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		if (item.getItemId() == android.R.id.home) {
			onBackPressed();
			result = true;
		}
		return result;
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(0, 0);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		fragment = getFragmentManager().findFragmentById(R.id.layout_container);
		if (fragment != null) {
			fragment.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	@Override
	public void searchText(String text) {
		startSearching(text);
		DatabaseProvider.getInstance().addSearchKeyword(text);
	}
	
	private void initialize() {		
		this.configureActionBarWithSearch();
		int duration = 0;
		if (!this.isAnimationStarted) {
			duration = getDuration();
			this.isAnimationStarted = true;
		}  
		this.createAnimation();
		this.startAnimation(duration);
	}
	
	private void configureActionBarWithSearch() {
		this.searchBar = new SearchBarView(this);
		final ActionBar actionBar = this.getSupportActionBar();
		actionBar.setCustomView(this.searchBar, new ActionBar.LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT,
				Gravity.START | Gravity.CENTER_VERTICAL));
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_HOME_AS_UP);
		if (this.searchBar != null) {
			this.searchBar.setSearchListener(this);
			this.searchBar.setFocus();
		}
	}
	
	private void startSearching(String query) {
		FragmentManager fragmentManager = getFragmentManager();
		fragment = fragmentManager.findFragmentByTag(query);
		if (fragment == null) {
			fragment = SearchFragment.newInstance(query);
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.layout_container, fragment, query);
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fragmentTransaction.commit();
		}
	}
	
	private void createAnimation() {
		int color = Color.argb(150, 0, 0, 0);
		this.backgroundAlphaAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), Color.TRANSPARENT, color);
		this.backgroundAlphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
		    @Override
		    public void onAnimationUpdate(ValueAnimator animator) {
		    	layoutRoot.setBackgroundColor((Integer)animator.getAnimatedValue()); 
		    }
		});
		this.backgroundAlphaAnimator.setInterpolator(getInterpolator());
		
		int deviceCurrentWidth = SizeUtil.getSizeInPixels(getResources().getConfiguration().screenWidthDp);
		searchBarTranslateAnimation = ObjectAnimator.ofFloat(layoutMain, View.TRANSLATION_X, deviceCurrentWidth, 0f);
		searchBarTranslateAnimation.setInterpolator(getInterpolator());
	}
	
	/**
	 * This method removes views with animation, and finish the activity when animation ends. 
	 */
	private void reverseAnimation() {
		this.searchBarTranslateAnimation.setDuration(getDuration());
		this.searchBarTranslateAnimation.reverse();
		this.backgroundAlphaAnimator.setDuration(getDuration());
		this.backgroundAlphaAnimator.reverse();
		
		this.backgroundAlphaAnimator.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationCancel(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				// finish activity after animation ends
				SearchActivity.super.finish();
				overridePendingTransition(0, 0);
			}

			@Override
			public void onAnimationRepeat(Animator animation) {	
			}

			@Override
			public void onAnimationStart(Animator animation) {	
			}
		});
	}
	
	private void startAnimation(int duration) {
		this.backgroundAlphaAnimator.setDuration(duration);
		this.backgroundAlphaAnimator.start();
		this.searchBarTranslateAnimation.setDuration(duration);
		this.searchBarTranslateAnimation.start();
	}
	
	private int getDuration() {
		return 250;
	}
	
	private Interpolator getInterpolator() {
		return new AccelerateDecelerateInterpolator();
	}
	
}
