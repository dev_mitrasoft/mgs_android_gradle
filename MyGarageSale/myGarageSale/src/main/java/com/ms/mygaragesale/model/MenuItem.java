package com.ms.mygaragesale.model;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;

import com.ms.garagesaledeal.R;

public class MenuItem {

	private static ArrayList<MenuItemGroup> menuItemGroupsList;
	private static HashMap<MenuItemGroup, ArrayList<MenuItemChild>> menuItemsMap;

	public static ArrayList<MenuItemGroup> getMenuItemGroupsList() {
		if (MenuItem.menuItemGroupsList == null) {
			MenuItem.menuItemGroupsList = new ArrayList<>();
		} 
		MenuItem.menuItemGroupsList.clear();
		MenuItem.menuItemGroupsList.add(MenuItemGroup.ACCOUNT);
		MenuItem.menuItemGroupsList.add(MenuItemGroup.MESSAGES);
		// Hiding request since user account is by default activated.
//		if (CurrentUser.getCurrentUser().getUserRole() != User.USER_ROLE_USER) {
//			MenuItem.menuItemGroupsList.add(MenuItemGroup.REQUESTS);
////			MenuItem.menuItemGroupsList.add(MenuItemGroup.CATEGORIES);
//		}
		MenuItem.menuItemGroupsList.add(MenuItemGroup.ALL_POSTS);
		MenuItem.menuItemGroupsList.add(MenuItemGroup.MY_POSTS);
		if (CurrentUser.getCurrentUser().getUserRole() == User.USER_ROLE_ADMIN) {
			MenuItem.menuItemGroupsList.add(MenuItemGroup.SPONSORED_POSTS);
		}
		// TODO uncomment when require
//		if (CurrentUser.getCurrentUser().getAccountType() == User.ACCOUNT_TYPE_FACEBOOK) {
//			MenuItem.menuItemGroupsList.add(MenuItemGroup.FB_POSTS);
//		}
		MenuItem.menuItemGroupsList.add(MenuItemGroup.ITEM_ALERTS);
		MenuItem.menuItemGroupsList.add(MenuItemGroup.LOGOUT);
		//MenuItem.menuItemGroupsList.add(MenuItemGroup.SETTINGS);
		return MenuItem.menuItemGroupsList;
	}

	public static HashMap<MenuItemGroup, ArrayList<MenuItemChild>> getMenuItemsMap() {
		if (MenuItem.menuItemsMap == null) {
			MenuItem.menuItemsMap = new HashMap<MenuItemGroup, ArrayList<MenuItemChild>>();
		}
		MenuItem.menuItemsMap.clear();
			
		for (MenuItemGroup menuItemGroup : menuItemGroupsList) {
			ArrayList<MenuItemChild> childs = new ArrayList<>();
			switch (menuItemGroup) {
				case ACCOUNT: 
					childs.add(MenuItemChild.ACCOUNT);
					break;
				case ALL_POSTS:
					if (CurrentUser.getCurrentUser().getUserRole() == User.USER_ROLE_USER) {
						childs.add(MenuItemChild.ALL_POSTS);
					} else {
						childs.add(MenuItemChild.ALL_POSTS);
//						childs.add(MenuItemChild.ALL_PENDING_POSTS);
//						childs.add(MenuItemChild.ALL_OPEN_POSTS);
//						childs.add(MenuItemChild.ALL_PENDING_POSTS);
//						childs.add(MenuItemChild.ALL_SOLD_POSTS);
//						childs.add(MenuItemChild.ALL_REJECTED_POSTS);
					}
					break;
				case MY_POSTS:
					childs.add(MenuItemChild.MY_POSTS);
//					childs.add(MenuItemChild.MY_OPEN_POSTS);
//					childs.add(MenuItemChild.MY_PENDING_POSTS);
//					childs.add(MenuItemChild.MY_SOLD_POSTS);
//					childs.add(MenuItemChild.MY_REJECTED_POSTS);
					break;
				case MESSAGES:
					childs.add(MenuItemChild.MESSAGES);
					break;
				case REQUESTS:
					childs.add(MenuItemChild.REQUESTS);
					break;
				case SETTINGS:
					childs.add(MenuItemChild.SETTINGS);
					break;
				case CATEGORIES:
					childs.add(MenuItemChild.CATEGORIES);
					break;
				case LOGOUT:
					childs.add(MenuItemChild.LOGOUT);
					break;
				case FB_POSTS:
					childs.add(MenuItemChild.FB_POSTS);
					break;
				case SPONSORED_POSTS:
					childs.add(MenuItemChild.SPONSORED_POSTS);
					break;
				case ITEM_ALERTS:
					childs.add(MenuItemChild.ALERT_KEYWORDS);
					break;
			}
			menuItemsMap.put(menuItemGroup, childs);
		}
		return MenuItem.menuItemsMap;
	}
	
	public static String getGroupTitle(MenuItemGroup menuItemGroup, Context context) {
		String groupTitle = "";
		switch (menuItemGroup) {
		case ACCOUNT:
			groupTitle = context.getResources().getString(R.string.menu_item_account);
			break;
		case ALL_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_all_posts);
			break;
		case MY_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_posts);
			break;
		case MESSAGES:
			groupTitle = context.getResources().getString(R.string.menu_item_message);
			break;
		case REQUESTS:
			groupTitle = context.getResources().getString(R.string.menu_item_requests);
			break;
		case SETTINGS:
			groupTitle = context.getResources().getString(R.string.menu_item_setting);
			break;
		case CATEGORIES:
			groupTitle = context.getResources().getString(R.string.menu_item_category);
			break;
		case LOGOUT:
			groupTitle = context.getResources().getString(R.string.menu_item_logout);
			break;
		case FB_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_fb_posts);
			break;
		case SPONSORED_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_sponsored_posts);
			break;
		case ITEM_ALERTS:
			groupTitle = context.getResources().getString(R.string.menu_item_alerts);
			break;
		}
		return groupTitle;
	}
	
	public static int getGroupLayout(MenuItemGroup menuItemGroup) {
		int groupLayout = R.layout.view_menu_items_group_blank;
		switch (menuItemGroup) {
		case ACCOUNT:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
		case ALL_POSTS:
//			if (CurrentUser.getCurrentUser().getUserRole() == User.USER_ROLE_USER) {
//				groupLayout = R.layout.view_menu_items_group_blank;
//			} else {
//				groupLayout = R.layout.view_menu_items_group_with_title;
//			}
			groupLayout = R.layout.view_menu_items_group_without_title;
			break;
		case MY_POSTS:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
		case MESSAGES:
			groupLayout = R.layout.view_menu_items_group_only_space;
			break;
		case REQUESTS:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
		case SETTINGS:
			groupLayout = R.layout.view_menu_items_group_without_title;
			break;
		case CATEGORIES:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
		case ITEM_ALERTS:
			groupLayout = R.layout.view_menu_items_group_without_title;
			break;
		case LOGOUT:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
		case FB_POSTS:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
		case SPONSORED_POSTS:
			groupLayout = R.layout.view_menu_items_group_blank;
			break;
			
		}
		return groupLayout;
	}
	
	public static String getChildTitle(MenuItemChild menuItemChild, Context context) {
		String groupTitle = "";
		switch (menuItemChild) {
		case ACCOUNT:
			groupTitle = context.getResources().getString(R.string.menu_item_account);
			break;
		case ALL_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_all_posts);
			break;
		case ALL_OPEN_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_open_posts);
			break;
		case ALL_PENDING_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_pending_posts);
			break;
		case ALL_SOLD_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_sold_posts);
			break;
		case ALL_REJECTED_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_rejected_posts);
			break;
		case MY_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_posts);
			break;
		case MY_OPEN_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_open_posts);
			break;
		case MY_PENDING_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_pending_posts);
			break;
		case MY_REJECTED_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_rejected_posts);
			break;
		case MY_SOLD_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_my_sold_posts);
			break;
		case MESSAGES:
			groupTitle = context.getResources().getString(R.string.menu_item_message);
			break;
		case REQUESTS:
			groupTitle = context.getResources().getString(R.string.menu_item_requests);
			break;
		case SETTINGS:
			groupTitle = context.getResources().getString(R.string.menu_item_setting);
			break;
		case CATEGORIES:
			groupTitle = context.getResources().getString(R.string.menu_item_category);
			break;
		case LOGOUT:
			groupTitle = context.getResources().getString(R.string.menu_item_logout);
			break;
		case FB_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_fb_posts);
			break;
		case SPONSORED_POSTS:
			groupTitle = context.getResources().getString(R.string.menu_item_sponsored_posts);
			break;
			case ALERT_KEYWORDS:
			groupTitle = context.getResources().getString(R.string.menu_item_alerts);
			break;
		}
		return groupTitle;
	}
	
	public static int getChildIcon(MenuItemChild menuItemChild) {
		int childIcon = 0;
		switch (menuItemChild) {
		case ACCOUNT:
			childIcon = R.drawable.ic_anonymous_avatar_40dp;
			break;
		case ALL_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case ALL_OPEN_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case ALL_PENDING_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case ALL_SOLD_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case ALL_REJECTED_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case MY_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case MY_OPEN_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case MY_PENDING_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case MY_REJECTED_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case MY_SOLD_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case MESSAGES:
			childIcon = R.drawable.ic_message_grey600_24dp;
			break;
		case REQUESTS:
			childIcon = R.drawable.ic_assignment_returned_grey600_24dp;
			break;
		case SETTINGS:
			childIcon = R.drawable.ic_menu_item_settings;
			break;
		case CATEGORIES:
			childIcon = R.drawable.ic_menu_item_settings;
			break;
		case LOGOUT:
			childIcon = R.drawable.ic_https_grey600_24dp;
			break;
		case FB_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case SPONSORED_POSTS:
			childIcon = R.drawable.ic_shopping_basket_grey600_24dp;
			break;
		case ALERT_KEYWORDS:
			childIcon = R.drawable.ic_visibility_grey600_24dp;
			break;
		}
		return childIcon;
	}
	
	public static int getChildIconSel(MenuItemChild menuItemChild) {
		int childIcon = 0;
		switch (menuItemChild) {
		case ACCOUNT:
			childIcon = R.drawable.ic_anonymous_avatar_40dp;
			break;
		case ALL_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case ALL_OPEN_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case ALL_PENDING_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case ALL_SOLD_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case ALL_REJECTED_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case MY_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case MY_OPEN_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case MY_PENDING_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case MY_REJECTED_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case MY_SOLD_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case MESSAGES:
			childIcon = R.drawable.ic_message_black_24dp;
			break;
		case REQUESTS:
			childIcon = R.drawable.ic_assignment_returned_black_24dp;
			break;
		case SETTINGS:
			childIcon = R.drawable.ic_menu_item_settings;
			break;
		case CATEGORIES:
			childIcon = R.drawable.ic_menu_item_settings;
			break;
		case LOGOUT:
			childIcon = R.drawable.ic_https_black_24dp;
			break;
		case FB_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case SPONSORED_POSTS:
			childIcon = R.drawable.ic_shopping_basket_black_24dp;
			break;
		case ALERT_KEYWORDS:
			childIcon = R.drawable.ic_visibility_black_24dp;
			break;
		}
		return childIcon;
	}
	
	public static int getChildLayout(MenuItemChild menuItemChild) {
		int groupLayout = R.layout.view_menu_items_child;
		if (menuItemChild == MenuItemChild.ACCOUNT) {
			groupLayout = R.layout.view_menu_items_child_my_info;
		}
		return groupLayout;
	}

}
