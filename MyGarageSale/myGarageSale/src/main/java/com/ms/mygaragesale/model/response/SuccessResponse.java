package com.ms.mygaragesale.model.response;

public class SuccessResponse extends Response {
	
	private boolean success;

	public SuccessResponse(int errorCode, String errorMessage, boolean success) {
		super(errorCode, errorMessage);
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
