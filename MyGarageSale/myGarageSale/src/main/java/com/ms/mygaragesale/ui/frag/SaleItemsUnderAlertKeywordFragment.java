package com.ms.mygaragesale.ui.frag;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.ms.mygaragesale.core.Constants;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.util.NotificationUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.response.SaleItemsResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;


public class SaleItemsUnderAlertKeywordFragment extends SaleItemsFragment {

    private AlertKeyword mAlertKeyword;

    public static SaleItemsUnderAlertKeywordFragment newInstance(AlertKeyword alertKeyword) {
        final SaleItemsUnderAlertKeywordFragment fragment = new SaleItemsUnderAlertKeywordFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ActivityBundleFactory.KEY_OBJECT, alertKeyword);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        NotificationUtil.resetItemAlertsNotification();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title = "Alert Items";
        if (mAlertKeyword != null) {
            title += " ("+ mAlertKeyword.getKeywordName() +")";
        }
        ((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(title);
        timestampViewLastUpdated.setVisibility(View.GONE);
        this.hideFloatingActionButton();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        // no option menu require
        menu.clear();
    }

    @Override
    protected void update() {
        super.update();
        this.hideFloatingActionButton();
    }

    protected void fetchArguments() {
        super.fetchArguments();
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ActivityBundleFactory.KEY_OBJECT)) {
            mAlertKeyword = (AlertKeyword) bundle.getSerializable(ActivityBundleFactory.KEY_OBJECT);
        }
    }

    @Override
    protected long getLastUpdatedTime() {
        return 0;
    }

    @Override
    protected void setLastUpdatedTimeStamp(long lastUpdatedTime) {
        // do nothing
    }

    protected void getSaleItemFromApi(boolean shouldResultCacheResponse) {
        String url = null;
        if (mAlertKeyword != null) {
            url = UrlUtil.getSaleItemUnderAlertKeywordUrl(mAlertKeyword.getKeywordName());
        } else {
            // if mAlertKeyword is null, load all alert items under users alert keywords
            url = UrlUtil.getSaleItemUnderAlertKeywordsUrl();
        }
        saleItemApiTask = new ApiTaskFactory<SaleItemsResponse>().newInstance(getActivity(),
                url, ApiTask.APIMethod.GET, SaleItemsResponse.class, new GetSaleItemResponseHandler());
        saleItemApiTask.executeRequest(null, false);
    }
}
