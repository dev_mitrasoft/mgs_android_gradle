package com.ms.mygaragesale.ui.frag;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.request.AlertKeywordsRequest;
import com.ms.mygaragesale.model.response.AlertKeywordsResponse;
import com.ms.mygaragesale.model.response.SuccessResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.DetailActivity;
import com.ms.mygaragesale.ui.IActivityEventCallback;

import java.util.ArrayList;

public class AlertKeywordsListFragment extends RefreshableItemsWithCountHeaderFragment<AlertKeyword>
        implements OnClickListener, OnItemLongClickListener, IActivityEventCallback {

    private ArrayList<AlertKeyword> selectedAlertKeywords;
    private ApiTask<AlertKeywordsResponse> getAlertKeywordsApiTask;

    private ProgressDialog progressDialog;
    private ActionMode actionMode;
    private boolean isInActionMode;


    public static AlertKeywordsListFragment newInstance() {
        return new AlertKeywordsListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        // refresh alert keywords from server
        getAlertKeywordsFromApi(false);
    }

    @Override
    public boolean onActivityBackPressed() {
        if (isInActionMode) {
            this.actionMode.finish();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getAlertKeywordsApiTask != null) {
            getAlertKeywordsApiTask.cancel();
        }
    }

    @Override
    protected void update() {
        listItems.clear();
        listItems.addAll(DatabaseProvider.getInstance().getAlertKeywords());
        super.update();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
        AlertKeyword selectedKeyword = listItems.get(index - 1);
        if (selectedAlertKeywords != null) {
            // do not select item if it is not downloaded
            // else toggle item selection
            if (selectedAlertKeywords.contains(selectedKeyword)) {
                selectedAlertKeywords.remove(selectedKeyword);
                absListView.setItemChecked(index, false);
            } else {
                selectedAlertKeywords.add(selectedKeyword);
                absListView.setItemChecked(index, true);
            }
            actionMode.invalidate();
        } else {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            Bundle bundle = ActivityBundleFactory.getBundle(null, selectedKeyword, ActivityBundleFactory.ViewType.ALERT_KEYWORD_SALE_ITEM);
            intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
            startActivity(intent);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        if (selectedAlertKeywords == null) {
            selectedAlertKeywords = new ArrayList<AlertKeyword>();
            this.deselectListItemsView();
            this.actionMode = ((ActionBarActivity) getActivity()).getSupportActionBar().startActionMode(new MultiSelectionActionModeCallback());
        }
        return false;
    }

    @Override
    public void onTapToRetry() {
        super.onTapToRetry();
        getAlertKeywordsFromApi(true);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        getAlertKeywordsFromApi(false);
    }


    @Override
    protected AbsListView getAbsListView() {
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        AbsListView listView = (AbsListView) layoutInflater.inflate(R.layout.list_view_messages, null);
        listView.setOnItemLongClickListener(this);
        return listView;
    }

    @Override
    protected void initialize() {
        super.initialize();
        swipeRefreshLayout.setEnabled(true);
        hideFloatingActionButton();
        update();

        // show action mode, called here to display action mode after orientation changes.
        if (isInActionMode) {
            if (this.actionMode != null) {
                ArrayList<AlertKeyword> selectedItemsCopy = new ArrayList<>(selectedAlertKeywords);
                this.actionMode.finish();
                selectedAlertKeywords = selectedItemsCopy;
            }
            this.actionMode = ((ActionBarActivity) getActivity()).getSupportActionBar().startActionMode(new MultiSelectionActionModeCallback());
            actionMode.invalidate();
        }
    }

    @Override
    protected long getLastUpdatedTime() {
        return new AppSharedPref().getAlertKeywordsLastUpdatedTime();
    }

    @Override
    protected View getItemView(int position, final AlertKeyword item, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.view_categories, parent, false);
        }
        if (isInActionMode) {
            convertView.setBackgroundResource(R.drawable.new_selector_button_flat_activated);
        } else {
            convertView.setBackgroundResource(R.drawable.new_selector_button_flat);
        }

        TextView textViewCategoryName = (TextView) convertView.findViewById(R.id.text_view_alert_keyword_name);
        textViewCategoryName.setText(item.getKeywordName());

        ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.image_view_category_icon);
        imageViewIcon.setImageResource(item.getKeywordType() == AlertKeyword.ALERT_KEYWORD_TYPE_SEARCH ? R.drawable.ic_history_grey600_18dp : R.drawable.ic_local_offer_grey600_18dp);

        ImageButton imageButtonRemoveKeyword = (ImageButton) convertView.findViewById(R.id.image_button_remove_keyword);
        if (isInActionMode) {
            imageButtonRemoveKeyword.setVisibility(View.GONE);
        } else {
            imageButtonRemoveKeyword.setVisibility(View.VISIBLE);
        }
        imageButtonRemoveKeyword.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AlertKeyword> selectedKeywords = new ArrayList<AlertKeyword>(1);
                selectedKeywords.add(item);
                showDeletePromptDialog(selectedKeywords);
            }
        });

        return convertView;
    }

    @Override
    protected void displayNoItemsMessage() {
        messageView.showMessage("No Keywords Found", R.drawable.ic_visibility_grey600_48dp, false);
    }

    private void deselectListItemsView() {
        for(int i = 0; i < absListView.getCount(); i++) {
            absListView.setItemChecked(i, false);
        }
    }

    private void getAlertKeywordsFromApi(boolean shouldResultCacheResponse) {
        String url = null;
        url = UrlUtil.getAlertKeywordsUrl();
        getAlertKeywordsApiTask = new ApiTaskFactory<AlertKeywordsResponse>().newInstance(getActivity(),
                url, APIMethod.GET, AlertKeywordsResponse.class, new GetAlertKeywordsResponseHandler());
        getAlertKeywordsApiTask.executeRequest(null, shouldResultCacheResponse);
    }

    private void deleteSelectedCategories(ArrayList<AlertKeyword> listKeyword) {
        String url = null;
        url = UrlUtil.getDeleteAlertKeywordsUrl();
        ApiTask<SuccessResponse> deleteApiTask = new ApiTaskFactory<SuccessResponse>().newInstance(getActivity(),
                url, APIMethod.POST, SuccessResponse.class, new DeleteAlertKeywordsResponseHandler(listKeyword));
        AlertKeywordsRequest request = new AlertKeywordsRequest();
        request.setAlertKeywords(listKeyword);
        deleteApiTask.executeRequest(request, false);
        this.progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("removing keyword(s)...");
        progressDialog.show();
    }

    private class GetAlertKeywordsResponseHandler extends ApiResponseHandler<AlertKeywordsResponse> {

        @Override
        public void onApiSuccess(AlertKeywordsResponse response, boolean isCachedResponse) {
            super.onApiSuccess(response, isCachedResponse);
            getAlertKeywordsApiTask = null;
            error = null;
            if (isCachedResponse) {
                swipeRefreshLayout.setRefreshing(true);
            } else {
                swipeRefreshLayout.setRefreshing(false);
            }

            listItems.clear();
            if (!isCachedResponse) {
                new AppSharedPref().setAlertKeywordsLastUpdatedTime(System.currentTimeMillis());
                DatabaseProvider.getInstance().updateAlertKeywords(response.getAlertKeywords());
                if (response.getAlertKeywords() != null) {
                    listItems.addAll(response.getAlertKeywords());
                }
            }
            update();
        }

        @Override
        public void onApiFailure(MGSError error) {
            super.onApiFailure(error);
            getAlertKeywordsApiTask = null;
            swipeRefreshLayout.setRefreshing(false);
            AlertKeywordsListFragment.this.error = error;
            update();
        }
    }

    private class DeleteAlertKeywordsResponseHandler extends ApiResponseHandler<SuccessResponse> {

        private ArrayList<AlertKeyword> deleteAlertKeywords;

        DeleteAlertKeywordsResponseHandler(ArrayList<AlertKeyword> deleteAlertKeywords) {
            this.deleteAlertKeywords = deleteAlertKeywords;
        }

        @Override
        public void onApiSuccess(SuccessResponse response, boolean isCachedResponse) {
            super.onApiSuccess(response, isCachedResponse);
            if (!isCachedResponse && response.isSuccess()) {
                if (getActivity() != null && progressDialog != null) {
                    progressDialog.dismiss();
                }

                DatabaseProvider.getInstance().deleteAlertKeywords(deleteAlertKeywords);

                // removing deleted list keywords
                listItems.removeAll(deleteAlertKeywords);

                update();
                MGSApplication.showToast("Keyword(s) removed from watch list.", Toast.LENGTH_LONG);
            }
        }

        @Override
        public void onApiFailure(MGSError error) {
            super.onApiFailure(error);
            if (getActivity() != null && progressDialog != null) {
                progressDialog.dismiss();
            }

            MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
        }

    }

    private void showDeletePromptDialog(final ArrayList<AlertKeyword> selectedAlertKeywords) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Remove");
        builder.setMessage("Do you want to remove selected alert keyword(s) from watch list?");
        builder.setPositiveButton("REMOVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayList<AlertKeyword> listKeyword = new ArrayList<>(selectedAlertKeywords);
                deleteSelectedCategories(listKeyword);
                if (actionMode != null) {
                    actionMode.finish();
                }
            }
        });
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (actionMode != null) {
                    actionMode.finish();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private class MultiSelectionActionModeCallback implements android.support.v7.view.ActionMode.Callback {

        @Override
        public boolean onActionItemClicked(android.support.v7.view.ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    showDeletePromptDialog(AlertKeywordsListFragment.this.selectedAlertKeywords);
                    return true;
            }
            return false;
        }

        @Override
        public boolean onCreateActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            isInActionMode = true;
            mode.getMenuInflater().inflate(R.menu.cab_category, menu);
            absListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            // to change selector with state activated
            itemsAdapter.notifyDataSetChanged();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //set your gray color
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.theme_action_mode_color_dark));
            }

            return true;
        }

        @Override
        public void onDestroyActionMode(android.support.v7.view.ActionMode mode) {
            isInActionMode = false;
            selectedAlertKeywords.clear();
            selectedAlertKeywords = null;
            deselectListItemsView();
            absListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // to change selector without state activated
            itemsAdapter.notifyDataSetChanged();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //set your gray color
                getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.theme_color_dark));
            }

            mode = null;
            actionMode = null;
        }

        @Override
        public boolean onPrepareActionMode(android.support.v7.view.ActionMode mode, Menu menu) {
            MenuItem menuDelete = menu.findItem(R.id.action_delete);
            if (selectedAlertKeywords.size() == 0) {
                setActionItemEnabled(menuDelete, false);
            } else {
                setActionItemEnabled(menuDelete, true);
            }

            mode.setTitle(selectedAlertKeywords.size() + " Selected");

            return false;
        }

        private void setActionItemEnabled(MenuItem menuItem, boolean enable) {
            menuItem.setEnabled(enable);
            Drawable drawable = menuItem.getIcon();
            if (drawable != null) {
                if (enable) {
                    drawable.setAlpha(255);
                } else {
                    drawable.setAlpha(100);
                }
            }
        }
    }

}
