package com.ms.mygaragesale.model.response;

import com.ms.mygaragesale.model.BaseItem;

public class Conversation implements BaseItem {

	private static final long serialVersionUID = 3069027723616327402L;
	
	public static final int CONVERSATION_TYPE_BETWEEN_USERS = 1;
	public static final int CONVERSATION_TYPE_IN_SALE_ITEM = 2;
   
    private String convId;  
    private ConversationEndInfo endOne;
    private ConversationEndInfo endAnother;
    private int convType;
    private LastMessageInConversation lastMessage;
    
    public Conversation() {
	}

	public String getConvId() {
		return convId;
	}

	public void setConvId(String convId) {
		this.convId = convId;
	}

	public ConversationEndInfo getEndOne() {
		return endOne;
	}

	public void setEndOne(ConversationEndInfo endOne) {
		this.endOne = endOne;
	}

	public ConversationEndInfo getEndAnother() {
		return endAnother;
	}

	public void setEndAnother(ConversationEndInfo endAnother) {
		this.endAnother = endAnother;
	}

	public int getConvType() {
		return convType;
	}

	public void setConvType(int convType) {
		this.convType = convType;
	}

	public LastMessageInConversation getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(LastMessageInConversation lastMessage) {
		this.lastMessage = lastMessage;
	}
    
}
