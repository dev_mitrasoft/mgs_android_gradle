package com.ms.mygaragesale.model.response;

import java.io.Serializable;
import java.util.Date;

public class LastMessageInConversation implements Serializable {
	
	private static final long serialVersionUID = 5514585905591290148L;
	
	private String messageId;
	private String message;
	private String senderId;
	private String convId;
	private int messageType;
	private Date createdDate;
    
    public LastMessageInConversation() {
		// TODO Auto-generated constructor stub
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getConvId() {
		return convId;
	}

	public void setConvId(String convId) {
		this.convId = convId;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
    
}
