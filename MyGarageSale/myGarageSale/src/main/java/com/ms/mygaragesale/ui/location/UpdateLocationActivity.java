package com.ms.mygaragesale.ui.location;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.google.GLocation;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.DetailActivity;
import com.ms.mygaragesale.ui.MGSActivity;
import com.ms.mygaragesale.ui.frag.AddNewSaleItemFragment;
import com.ms.mygaragesale.ui.frag.EditUserDialogFragment;
import com.ms.mygaragesale.ui.frag.LoadingMessageDialogFragment;
import com.ms.mygaragesale.ui.view.LocationPredictionView;

public class UpdateLocationActivity extends MGSActivity implements View.OnClickListener, LoadingMessageDialogFragment.DialogFragmentCompleteListener {

	private LocationPredictionView mLocationPredictionView;
	private Button mButtonUpdateLocation;
	private EditUserDialogFragment mEditUserDialogFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_update_location);

		this.setupActionBar();
		mLocationPredictionView = (LocationPredictionView) findViewById(R.id.location_prediction_view);
		mLocationPredictionView.setEnabled(true);
		mButtonUpdateLocation = (Button) findViewById(R.id.button_update_location);
		mButtonUpdateLocation.setOnClickListener(this);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.finish();
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		if (mEditUserDialogFragment != null) {
			mEditUserDialogFragment.setDialogFragmentCompleteListener(null);
		}
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		String locDesc = mLocationPredictionView.getPlace().getDescription();
		if (!StringUtil.isNullOrEmpty(locDesc)) {
			CurrentUser currentUser = CurrentUser.getCurrentUser();
			currentUser.setLocation(new GLocation(mLocationPredictionView.getPlace()));
			currentUser.setDataSyncd(false);
			currentUser.updateCurrentUser();
			showEditUserDialog(false);
		} else {
			MGSApplication.showToast("Please enter location.", Toast.LENGTH_LONG);
		}
	}

	public void setupActionBar() {
		final ActionBar actionBar = this.getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayShowTitleEnabled(true);
			actionBar.setTitle("Update Your Location");
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public void onComplete(MGSError error) {
		if (mEditUserDialogFragment != null) {
			mEditUserDialogFragment.setDialogFragmentCompleteListener(null);
			mEditUserDialogFragment.dismiss();
		}
		if (error == null) {
			// launch add new sale item
			final Intent intent = new Intent(this, DetailActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable(ActivityBundleFactory.KEY_FRAGMENT_CLASS_NAME, AddNewSaleItemFragment.class);
			bundle.putBoolean(AddNewSaleItemFragment.KEY_CREATE_SPONSORED_ITEM, false);
			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
			this.startActivity(intent);
			this.finish();
		} else {
			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
		}
	}

	private void showEditUserDialog(boolean shouldUploadImage) {
		mEditUserDialogFragment = EditUserDialogFragment.newInstance(shouldUploadImage);
		mEditUserDialogFragment.setDialogFragmentCompleteListener(this);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment prev = fragmentManager.findFragmentByTag("dialog");
		if (prev != null) {
			transaction.remove(prev);
		}
		transaction.addToBackStack(null);
		mEditUserDialogFragment.show(transaction, "dialog");
	}
}
