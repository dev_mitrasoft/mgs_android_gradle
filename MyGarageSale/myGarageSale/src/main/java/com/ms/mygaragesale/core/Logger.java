package com.ms.mygaragesale.core;

import android.util.Log;

import com.ms.garagesaledeal.BuildConfig;

public class Logger {

	public static final boolean DEBUG = BuildConfig.DEBUG;

    public static void log(int priority, String tag, String msg) {
        if (Logger.DEBUG) {
            Log.println(priority, tag, msg);
        }
    }

	public static void log(String tag, String msg) {
		if (Logger.DEBUG) {
			Log.d(tag, msg);
		}
	}

	public static void log(String msg) {
		if (Logger.DEBUG) {
			Log.d("DEBUG", msg);
		}
	}
}
