package com.ms.mygaragesale.model;

import java.util.ArrayList;

import com.ms.mygaragesale.model.google.GPlace;
import com.ms.mygaragesale.model.response.Response;

// this is extended from Response since to use it with current ApiTask architecture.
public class LocationPrediction extends Response {

	private ArrayList<GPlace> predictions;

	public ArrayList<GPlace> getPredictions() {
		return predictions;
	}

	public void setPredictions(ArrayList<GPlace> predictions) {
		this.predictions = predictions;
	}
	
}
