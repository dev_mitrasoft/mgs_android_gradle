package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.model.Message;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;

public class MessageListFragment extends Fragment implements OnItemClickListener {
	
	private ListView listViewMessages;
	
	private MessageAdapter messageAdapter;
	private ArrayList<Message> listMessages;
	
	
    public static MessageListFragment newInstance() {
    	MessageListFragment fragment = new MessageListFragment();
        return fragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.setRetainInstance(true);
    	listMessages = new ArrayList<>();
//    	listMessages.add(new Message("as aspkcsdlk com wcow e", new Sender("1001", "Test user 1", false), new Date()));
//    	listMessages.add(new Message("as aspkcsdlk com wcow e", new Sender("1001", "Test user 1", false), new Date()));
//    	listMessages.add(new Message("as alkmno asdcqsxqw wcow e", new Sender("1021", "Test user", false), new Date()));
    	messageAdapter = new MessageAdapter();
    }

    public MessageListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_message_list, container, false);
        this.listViewMessages = (ListView) rootView.findViewById(R.id.list_view_messages);
        this.listViewMessages.setOnItemClickListener(this);
        listViewMessages.setAdapter(messageAdapter);
        return rootView;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	((ActionBarActivity) getActivity()).getSupportActionBar().setTitle("Post title");
    }

//	@Override
//	public void onClick(View view) {
//		if (view == imageButtonSendMessage) {
//			Me me = new Me();
//			Sender sender = new Sender(me.getId(), me.getName(), me.isModerator());
//			Message message = new Message(editTextWriteMessage.getText().toString(), sender, new Date());
//			listMessages.add(message);
//			editTextWriteMessage.setText("");
//			messageAdapter.notifyDataSetChanged();
//			listViewMessages.post(new Runnable() {
//				@Override
//				public void run() {
//					listViewMessages.smoothScrollToPosition(listMessages.size() - 1);
//				}
//			});
//		}
//	}
    
	
	private class MessageAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return listMessages.size();
		}

		@Override
		public Object getItem(int position) {
			return listMessages.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = layoutInflater.inflate(R.layout.view_message_list_items, parent, false);
			}
			TextView text_view_message_sender_name = (TextView) convertView.findViewById(R.id.text_view_message_sender_name);
			TextView text_view_message_date = (TextView) convertView.findViewById(R.id.text_view_message_date);
			TextView text_view_message = (TextView) convertView.findViewById(R.id.text_view_message);
			ImageView image_view_message_sender_icon = (ImageView) convertView.findViewById(R.id.image_view_message_sender_icon);
			
			//Message message = list
			
//			TextView textViewTitle = (TextView) convertView.findViewById(R.id.text_view_request_title);
//			TextView textViewSubtitle = (TextView) convertView.findViewById(R.id.text_view_request_subtitle);
//			ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.image_view_request_icon);
//			
//			if (position == 1) {
//				textViewTitle.setText("User name");
//				textViewSubtitle.setText("Creating new account");
//				imageViewIcon.setImageResource(R.drawable.ic_anonymous_avatar_40dp);
//			}
			return convertView;
		}
		
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Intent intent = new Intent(getActivity(), DetailActivity.class);
		Bundle bundle = ActivityBundleFactory.getBundle(null, null, ViewType.MESSAGE_IN_CONVERSATION);
		intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
		startActivity(intent);
	}

}
