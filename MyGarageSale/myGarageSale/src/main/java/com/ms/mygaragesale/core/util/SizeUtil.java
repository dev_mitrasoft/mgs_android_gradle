package com.ms.mygaragesale.core.util;

import com.ms.mygaragesale.core.MGSApplication;

public class SizeUtil {

	public static int getSizeInPixels(int sizeInDp) {
		float density = MGSApplication.getInstance().getApplicationContext().getResources().getDisplayMetrics().density;
		return (int)(density * sizeInDp);
	}
	
}
