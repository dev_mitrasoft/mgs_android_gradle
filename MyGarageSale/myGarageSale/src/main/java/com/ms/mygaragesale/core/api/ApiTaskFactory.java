package com.ms.mygaragesale.core.api;

import android.content.Context;

import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTask.IApiTaskResponseHandler;
import com.ms.mygaragesale.model.response.Response;

public class ApiTaskFactory<T extends Response> {
	
	public ApiTask<T> newInstance(Context context, String url, APIMethod method, Class<T> responseObjectClass, 
			IApiTaskResponseHandler<T> apiTaskResponseHandler) {
		return new VolleyApiTask<T>(context, url, method, responseObjectClass, apiTaskResponseHandler);
	}
	
	public ApiTask<T> newImageUploadInstance(Context context, String url, APIMethod method, 
			Class<T> responseObjectClass, IApiTaskResponseHandler<T> apiTaskResponseHandler) {
		return new ImageUploadTask<T>(context, url, method, responseObjectClass, apiTaskResponseHandler);
	}

}
