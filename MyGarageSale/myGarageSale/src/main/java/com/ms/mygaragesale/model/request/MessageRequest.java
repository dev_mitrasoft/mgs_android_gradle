package com.ms.mygaragesale.model.request;

import java.util.Date;

public class MessageRequest {
		
	private String message;
	private String messageId;
	private Date createdDate;
	private int messageType;
	private String senderId;
	private String receiverId;
	
	public MessageRequest() {
		// TODO Auto-generated constructor stub
	}

	public MessageRequest(String message, String messageId, Date createdDate,
			int messageType, String senderId, String receiverId) {
		super();
		this.message = message;
		this.messageId = messageId;
		this.createdDate = createdDate;
		this.messageType = messageType;
		this.senderId = senderId;
		this.receiverId = receiverId;
	}



	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}



	public String getSenderId() {
		return senderId;
	}



	public void setSenderId (String senderId) {
		this.senderId = senderId;
	}



	public String getReceiverId() {
		return receiverId;
	}



	public void setReceiverId(String receiverId) {
		this.receiverId = receiverId;
	}
	
	
}
