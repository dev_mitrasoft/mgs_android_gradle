package com.ms.mygaragesale.ui.frag;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewSwitcher;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.ui.view.FloatingActionButton;
import com.ms.mygaragesale.ui.view.LoadingView;
import com.ms.mygaragesale.ui.view.MessageView;
import com.ms.mygaragesale.ui.view.MessageView.OnMessageViewTapToRetryListener;

/**
 * Abstract base class to display common views like loading view, message view
 * and content view on landing page.
 */
public abstract class RefreshableFragment extends Fragment implements
		OnClickListener, OnRefreshListener, OnMessageViewTapToRetryListener {

	protected ViewSwitcher viewSwitcher;
	protected MessageView messageView;
	protected LoadingView loadingView;
	protected ViewGroup layoutOverlay;
	protected FloatingActionButton floatingActionButton;
	
	protected SwipeRefreshLayout swipeRefreshLayout;
	
	protected MGSError error;

	// this flag is used to restrict multiple calls to populate data from API
	// and to display savo loading view during orientation changes.
	protected boolean isRefreshing;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);
		this.setRetainInstance(true);
	}

	@Override
	public final View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_refreshable_items, null);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		viewSwitcher = (ViewSwitcher) view.findViewById(R.id.view_switcher);
		messageView = (MessageView) view.findViewById(R.id.message_view);
		messageView.setOnMessageViewTapToRetryListener(this);
		loadingView = (LoadingView) view.findViewById(R.id.loading_view);
		layoutOverlay = (ViewGroup) view.findViewById(R.id.layout_action_button);
		floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);
		swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
		swipeRefreshLayout.addView(this.getContentView());
				
		initView();
		initialize();
		return view;
	}
	
	private void initView() {
		floatingActionButton.setVisibility(View.GONE);
		floatingActionButton.setOnClickListener(this);
		swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.progress_bar_color_1), 
				getResources().getColor(R.color.progress_bar_color_2), getResources().getColor(R.color.progress_bar_color_3));
		swipeRefreshLayout.setOnRefreshListener(this);
		swipeRefreshLayout.setEnabled(false);
	}

	/**
	 * Override in child class to add view to display other than loading and
	 * message view. This method is called during initialization of view, i.e.
	 * in method initialized.
	 * 
	 * @return View - to be added to content view
	 */
	protected abstract View getContentView();
	
	protected abstract void initialize();
	
	@Override
	public void onClick(View view) {
	}
	

//		// retrieve data from saved state
//		//swipeRefreshLayout.setRefreshing(isRefreshing);
//		swipeRefreshLayout.post(new Runnable() {
//			
//			@Override
//			public void run() {
//				Logger.log("is refreshing : "+ isRefreshing);
//				swipeRefreshLayout.setRefreshing(isRefreshing);
//			}
//		});
	
	protected void setFloatingActionButtonIconResId(int resId) {
		floatingActionButton.setIcon(resId);
	}
	
	protected void showFloatingActionButton() {
		Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_out_action_button);
		animation.setAnimationListener(new Animation.AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				floatingActionButton.setVisibility(View.VISIBLE);
			}
		});
		floatingActionButton.startAnimation(animation);
		
	}
	
	protected void hideFloatingActionButton() {
		Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_in_action_button);
		animation.setAnimationListener(new Animation.AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				floatingActionButton.setVisibility(View.GONE);
			}
		});
		floatingActionButton.startAnimation(animation);
	}

	/**
	 * Method to switch between content views like loading, message and list view.
	 * 
	 * @param viewType
	 *            - {@link ContentViewType}
	 */
	protected void showView(ContentViewType viewType) {
		switch (viewType) {
		case CONTENT_LOADING:
			viewSwitcher.setDisplayedChild(ContentViewType.CONTENT_LOADING.getValue());
			break;
		case CONTENT_DATA:
			viewSwitcher.setDisplayedChild(ContentViewType.CONTENT_DATA.getValue());
			messageView.setVisibility(View.INVISIBLE);
			break;
		case CONTENT_MESSAGE:
			viewSwitcher.setDisplayedChild(ContentViewType.CONTENT_MESSAGE.getValue());
			messageView.setVisibility(View.VISIBLE);
			break;
		}
	}

//	private void showErrorMessage() {
//		if (error.getErrorType() == MGSErrorType.NETWORK_CONNECTION_ERROR) {
//			// messageView.showMessage(MessageType.OFFLINE);
//		} else {
//			// generic error
//			// savoMessageView.showMessage(getResources().getString(R.string.quick_start_items_blank_title),
//			// getResources().getString(R.string.quick_start_items_blank_description),
//			// R.drawable.quick_start_items_blank, MessageType.NO_RESULTS);
//		}
//	}
//
//	private void toastErrorMessage() {
//		String errorMessage = null;
//		if (error.getErrorType() == MGSErrorType.NETWORK_CONNECTION_ERROR) {
//			// errorMessage =
//			// getResources().getString(R.string.default_offline_toast);
//		} else {
//			// errorMessage =
//			// getResources().getString(R.string.default_error_toast);
//		}
//		Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
//	}

	/**
	 * Enum to indicate different content views.
	 */
	public enum ContentViewType {
		// index for CONTENT_DATA, CONTENT_MESSAGE are same since these both
		// views are displayed at same time using viewSwitcher.
		CONTENT_LOADING(0), CONTENT_DATA(1), CONTENT_MESSAGE(1);

		private int value;

		private ContentViewType(int value) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}
	
	@Override
	public void onTapToRetry() {
		
	}

	@Override
	public void onRefresh() {
		isRefreshing = true;
		Logger.log("onRefresh, isRefreshing: " + isRefreshing);
	}
}
