package com.ms.mygaragesale.model.response;

public class ImagesResponse extends Response {
	
	private IdResponse[] images;

	public ImagesResponse(int errorCode, String errorMessage, IdResponse[] images) {
		super(errorCode, errorMessage);
		this.images = images;
	}
	
	public IdResponse[] getImages() {
		return images;
	}
	
	public void setImages(IdResponse[] images) {
		this.images = images;
	}
}
