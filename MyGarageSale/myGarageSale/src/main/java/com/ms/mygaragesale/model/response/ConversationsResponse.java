package com.ms.mygaragesale.model.response;

import java.util.ArrayList;

public class ConversationsResponse extends Response {

	private int totalResults;
	private int limit;
	private int offset;
	private ArrayList<Conversation> conversations;
	
	public ConversationsResponse() {
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public ArrayList<Conversation> getConversations() {
		return conversations;
	}

	public void setConversations(ArrayList<Conversation> conversations) {
		this.conversations = conversations;
	}
		
}
