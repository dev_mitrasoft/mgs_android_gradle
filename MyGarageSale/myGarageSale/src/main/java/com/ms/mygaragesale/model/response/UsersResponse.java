package com.ms.mygaragesale.model.response;

import java.util.ArrayList;

import com.ms.mygaragesale.model.User;

public class UsersResponse extends Response {
	
	private int totalResults;
	private int limit;
	private int offset;
	private ArrayList<User> users;
	
	public UsersResponse() {
		// TODO Auto-generated constructor stub
	}

	public UsersResponse(int totalResults, int limit, int offset,
			ArrayList<User> users) {
		super();
		this.totalResults = totalResults;
		this.limit = limit;
		this.offset = offset;
		this.users = users;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public ArrayList<User> getUsers() {
		return users;
	}
	
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	
}
