package com.ms.mygaragesale.model.request;

import com.ms.mygaragesale.model.AlertKeyword;

import java.util.ArrayList;

public class AlertKeywordsRequest {

    private ArrayList<AlertKeyword> alertKeywords;

    public void setAlertKeywords(ArrayList<AlertKeyword> alertKeywords) {
        this.alertKeywords = alertKeywords;
    }

    public ArrayList<AlertKeyword> getAlertKeywords() {
        return alertKeywords;
    }
}
