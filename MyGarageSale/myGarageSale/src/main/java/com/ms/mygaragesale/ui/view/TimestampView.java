package com.ms.mygaragesale.ui.view;



import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ms.garagesaledeal.R;

public class TimestampView extends RelativeLayout 
{
	TextView timestampText;
	ImageView timestampIcon;
	
	public TimestampView(Context context, AttributeSet attrs, int defStyle) 
	{
		super(context, attrs, defStyle);
		init();
	}

	public TimestampView(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
		init();
	}

	public TimestampView(Context context) 
	{
		super(context);
		init();
	}
	
	@Override
	public void setVisibility(int visibility) {
		super.setVisibility(visibility);
		timestampText.setVisibility(visibility);
		timestampIcon.setVisibility(visibility);
	}

	private void init() 
	{
		inflate(getContext(), R.layout.view_timestamp, this);
		timestampText = (TextView) findViewById(R.id.timeStampText);
		timestampIcon = (ImageView) findViewById(R.id.timestampIcon);
	}
	
	public void setTimestamp(String text)
	{
		this.timestampText.setText(text);
	}
}
