package com.ms.mygaragesale.model.google;

public class GGeometry {

	private GCordinate location;
	
	public GCordinate getLocation() {
		return location;
	}
	
	public void setLocation(GCordinate location) {
		this.location = location;
	}
	
}
