package com.ms.mygaragesale.core.util;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class EndlessAdapter extends BaseAdapter {

    // flag to store data availability, used to display loading view
	private boolean isDataAvailable;
    // flag to prevent multiple load more call to same position until user notifyDataSetChanged or notifyDataSetInvalidated
    private int loadMoreLastPosition = -1;
	private int count;

	@Override
	public final int getCount() {
		this.isDataAvailable = isMoreDataAvailable();
		if (isDataAvailable) {
			this.count = this.getItemCount() + 1;
		} else {
			this.count = this.getItemCount();
		}
		return this.count;
	}

	@Override
	public final Object getItem(int position) {
		return null;
	}

	@Override
	public final long getItemId(int position) {
		return 0;
	}

	@Override
	public final View getView(int position, View convertView, ViewGroup parent) {
		if (this.isDataAvailable && position == this.count - 1) {
			View view = this.getProgressView();
            if (this.loadMoreLastPosition != position) {
                this.loadMoreLastPosition = position;
                this.loadMoreData();
            }
			return view;
		}
		return this.getItemView(position, convertView, parent);
	}

    @Override
    public void notifyDataSetChanged() {
        this.loadMoreLastPosition = -1;
        super.notifyDataSetChanged();
    }

    @Override
    public void notifyDataSetInvalidated() {
        this.loadMoreLastPosition = -1;
        super.notifyDataSetInvalidated();
    }

    public abstract int getItemCount();
	
	public abstract View getItemView(int position, View convertView, ViewGroup parent);
	
	public abstract boolean isMoreDataAvailable();
	
	public abstract View getProgressView();
	
	public abstract void loadMoreData();
	
}
