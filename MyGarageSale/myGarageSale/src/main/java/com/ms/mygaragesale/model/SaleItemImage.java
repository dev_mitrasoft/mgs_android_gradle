//package com.ms.mygaragesale.model;
//
//public class SaleItemImage implements BaseItem {
//
//    private Long id;
//    private String imageId;
//    private String imageUri;
//    private Long saleItemId;
//
//    public SaleItemImage() {
//    }
//
//    public SaleItemImage(Long id) {
//        this.id = id;
//    }
//
//    public SaleItemImage(Long id, String imageId, String imageUri, Long saleItemId) {
//        this.id = id;
//        this.imageId = imageId;
//        this.imageUri = imageUri;
//    }
//    
//    public SaleItemImage(com.ms.mygaragesale.core.db.dao.SaleItemImageTable dbSaleItemImage) {
//    	 this.id = dbSaleItemImage.getId();
//         this.imageId = dbSaleItemImage.getImageId();
//         this.imageUri = dbSaleItemImage.getImageUri();
//         this.saleItemId = dbSaleItemImage.getSaleItemId();
//    }
//    
//    public com.ms.mygaragesale.core.db.dao.SaleItemImageTable getDbSaleItemImage() {
//    	com.ms.mygaragesale.core.db.dao.SaleItemImageTable saleItemImage = new com.ms.mygaragesale.core.db.dao.SaleItemImageTable(id, imageId, imageUri, saleItemId);
//    	return saleItemImage;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getImageId() {
//        return imageId;
//    }
//
//    public void setImageId(String imageId) {
//        this.imageId = imageId;
//    }
//
//    public String getImageUri() {
//        return imageUri;
//    }
//
//    public void setImageUri(String imageUri) {
//        this.imageUri = imageUri;
//    }
//
//}
