package com.ms.mygaragesale.core;

import com.ms.garagesaledeal.R;

public class MGSError {
	
	private String message;
	private MGSErrorType errorType;
	
	public MGSError() {
		this.errorType = MGSErrorType.ERROR;
		this.message = MGSApplication.getInstance().getApplicationContext().getResources().getString(R.string.error);
	}
	
	public MGSError(MGSErrorType errorType) {
		this.errorType = errorType;
		this.message = this.getErrorMessage();
	}
	
	public MGSError(MGSErrorType errorType, String message) {
		this.errorType = errorType;
		this.message = message;
	}
	
	public String getMesssage() {
		return this.message;
	}
	
	public MGSErrorType getErrorType() {
		return this.errorType;
	}
	
	private String getErrorMessage() {
		String errorMessage = MGSApplication.getInstance().getApplicationContext().getResources().getString(R.string.error);
		switch (this.errorType) {
			case NO_NETWORK_CONNECTION:
				errorMessage = MGSApplication.getInstance().getApplicationContext().getResources().getString(R.string.no_internet_connection);
				break;
			case LOCATION_UNAVAILABLE_ERROR:
				errorMessage = MGSApplication.getInstance().getApplicationContext().getResources().getString(R.string.location_unavailable);
				break;
			case API_ERROR:			
			case ERROR:
			default:
				errorMessage = MGSApplication.getInstance().getApplicationContext().getResources().getString(R.string.error);
				break;
		}
		return errorMessage;
	}
		
	@Override
	public boolean equals(Object obj) {
		return this.errorType == ((MGSError) obj).errorType;
	}
	
	
	
	public static enum MGSErrorType {
		ERROR,
		NO_NETWORK_CONNECTION,
		UNAUTHORIZED_ERROR,
		CONFLICT_ERROR,
		API_ERROR,
		FB_LOGIN_ERROR,
		TWITTER_LOGIN_ERROR,
		NO_USER_LOGGED_IN,
		USER_ACCOUNT_BLOCKED,
		LOCATION_UNAVAILABLE_ERROR
	}

}
